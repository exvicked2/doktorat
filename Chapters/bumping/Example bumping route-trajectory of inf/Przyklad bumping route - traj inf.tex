\documentclass[10pt]{beamer}
\setbeamertemplate{items}[ball]

\mode<presentation>
{
	\usetheme{Warsaw}
\setbeamercovered{transparent}
}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[labelfont={normalsize}]{caption,subfig}
\usepackage{units}
\usepackage{commath}

\usepackage{subfiles}

\usepackage[backgroundcolor=yellow]{todonotes}

\usepackage{csquotes}

\usepackage{color}
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.markings}

\definecolor{darkgreen}{rgb}{0.0, 0.2, 0.13}

\newcommand{\on}{\operatorname}
\newcommand{\pos}{\on{pos}}
\newcommand{\sh}{\on{sh}}
\newcommand{\traj}{\on{traj}}
\newcommand{\Pos}{\on{Pos}}
\newcommand{\lazy}{\on{lazy}}

\makeatletter 
	\renewcommand\d[1]{\ensuremath{%
		\;\mathrm{d}#1\@ifnextchar\d{\!}{}}}
\makeatother

\newcommand{\E}{\mathbb{E}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\Young}{\mathbb{Y}}

\newcommand{\Gl}{\mathfrak{Gl}}
\newcommand{\Hom}{\on{Hom}}
\newcommand{\sgn}{\on{sgn}}
\newcommand{\RSK}{\on{RSK}}


\newcommand{\Prob}{\mathbb{P}}


\newcommand{\tableaux}{\mathcal{T}}
\newcommand{\pieritableaux}{\widetilde{\mathcal{T}}}

\newcommand{\tab}{T}
\newcommand{\multi}{M}
\newcommand{\water}{w}

\newcommand{\ttt}{\mathbb{T}'_N}
\newcommand{\mmm}{\mathbb{M}'_N}
\newcommand{\www}{\mathbb{W}'_N}
\newcommand{\F}{\mathcal{F}}

\author[Łukasz Maślanka]{Łukasz Maślanka}

\institute[IMPAN]
{Instytut Matematyczny Polskiej Akademii Nauk}

\title[On bumping route and trajectory of $\infty$]
{The correspondence between \\
the~bumping route 
in~the~recording tableau \\
and trajectory of $\infty$
in the~insertion tableau}

\date{
18.01.2021
}

%\includeonlyframes{murl}


\begin{document}

%
%		Strona tytułowa
%
	\frame{\titlepage}




%%%%%%%%%%
% Krok indukcyjny w algorytmie Robinsona-Schensteda
%%%%%%%%%%

\begin{frame}
\frametitle{Robinson-Schensted-Knuth algorithm -- induction step}

\input{rsk-animacja-krok-indukcyjny.tex} 

\end{frame}

%%%%%%%%%%
% Twierdzenie o algorytmie Robinsona-Schensteda
%%%%%%%%%%

\begin{frame}
\frametitle{$\RSK$ gives a bijection}

\begin{alertblock}{$\RSK$ gives a bijection}
$\RSK$ mapping is a bijection between $S_n$ 
and set of pairs $(P,Q)$ of standard Young tableaux 
with the same shape. 
\end{alertblock}

\end{frame}

%%%%%%%%%%
% Twierdzenie Maślanki-Marciniaka-Śniadego
%%%%%%%%%%

\begin{frame}
\frametitle{Marciniak-M.-Śniady Theorem, 2020}

\textbf{Apply RSK} algorithm to a sequence
\[ \xi_1,\dots,\xi_m, \infty, \xi_{m+1}, \xi_{m+2}, \dots\]

1. Consider \textbf{the~position of $\infty$}
\[
\Box^{\traj}_m(t)=
\Pos_\infty \big( P(\xi_1,\dots,\xi_m, \infty, \xi_{m+1}, \dots, \xi_t ) \big).
\]

2. Consider \textbf{the~bumping route} obtained by putting the number 
$m+\frac{1}{2}$ to the~recording tableau $Q(\xi_1, \xi_2, \ldots)$.
Focus on the box
\[   \Box^{\lazy}_{Q,m}(t) := \begin{array}{l} \textrm{the first box in the~bumping route which} \\ 
\textrm{contains an~entry of $Q$ which is bigger than $t$.}
\end{array}  \] 

\begin{alertblock}{Theorem, Marciniak-M.-Śniady, 2020}
Let $(\xi_n)$ be any sequence and $m\in \N$. For any integer $t \geq m$
\[
\Box^{\traj}_m(t) = \Box^{\lazy}_{Q,m}(t).
\] 
\end{alertblock}

\end{frame}


%%%%%%%%%%
% Algorytm Robinsona-Schensteda
%%%%%%%%%%

\begin{frame}
\frametitle{Robinson-Schensted-Knuth algorithm -- application}

\input{inf-trajectory-and-bumping.tex} 

\end{frame}


%%%%%%%%%%
% Obserwacje do twierdzenia MMŚ
%%%%%%%%%%

\begin{frame}
\frametitle{Crucial observation}

\begin{alertblock}{Observation}
The number $\infty$ is bumped in the~process of
%calculating the~row 
the insertion
    \begin{equation}
    \label{eq:indution}
    P(\xi_1,\dots,\xi_m, \infty, \xi_{m+1}, \dots, \xi_t ) \leftarrow \xi_{t+1} 
    \end{equation}
    \textbf{if and only if} 
		the~position of $\infty$ at time $t$, i.e.,~$\Box^{\traj}_m(t)$,
    is the~unique box which belongs to the~difference
    \[ 
    \RSK(\xi_1,\dots, \xi_{t+1})\setminus 
    \RSK(\xi_1,\dots, \xi_t ). \]
\end{alertblock}

\begin{alertblock}{Intuition(?)}
The trajectory of $\infty$ gives (exact+1) 
moments for bumping route in the~recording tableau. 
\end{alertblock}

\end{frame}



%%%%%%%%%%
% Krótki dowód obserwacji dla MMŚ
%%%%%%%%%%

\begin{frame}
\frametitle{Short proof}

\begin{alertblock}{Intuition(?)}
The trajectory of $\infty$ gives (exact+1) 
moments for bumping route in the~recording tableau. 
\end{alertblock}

Consider a sequence $\xi_1, \xi_2, \ldots$ (may be finite).
For any $n \in \{0, 1, 2, \dots\}$ let us define $m_{-1} := m$ and 
[the $n$-th jump of $\infty$]
\begin{multline*}
m_n := \min \left\{ t \geq m_{n-1}: \RSK(\xi_1, \dots, \xi_{t+1}) \setminus \RSK(\xi_1, \dots, \xi_t) \right\} \\
\textrm{ jest w $n$-tym wierszu}.
\end{multline*}

It is enough to show that $1 + m_n$ is the smallest entry $>m_{n-1}$
in $n$-th row in the~recording tableau. 

\begin{block}{Proof}
1. If $m_{n-1} < n < m_n+1$ then $Q(\xi_1, \dots, \xi_n) \setminus Q(\xi_1, \dots, \xi_{n-1})$ is not in row $n$; 

2. If $n > m_n+1$ then $n$ is not the smallest number $>m_{n-1}$ in the $n$-th row.
\end{block}

\end{frame}


%%%%%%%%%%
% Algorytm Robinsona-Schensteda
%%%%%%%%%%

\begin{frame}
\frametitle{Robinson-Schensted-Knuth algorithm -- application}

\input{inf-trajectory-and-bumping.tex} 

\end{frame}


\end{document}

