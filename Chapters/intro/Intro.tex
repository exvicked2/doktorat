% Chapter Template

\chapter{Introduction} % Main chapter title
\label{ch:intro} 

In this PhD thesis we investigate the asymptotics of two popular algorithms 
used in the study of Young diagrams and tableaux -- 
\emph{the Robinson--Schensted--Knuth algorithm} and 
\emph{the jeu de taquin algorithm}.
The results in this document
(see \cref{sec:content-intro} for their brief description) 
come from three collaborative papers
\cite{MMS-Poisson2020v2,MMS-Bumping2021-PTRF,MS-Evacuation2022v3}
(all with my supervisor, Piotr Śniady, 
and the first two with his another PhD student, Mikołaj Marciniak).
Each of the next three chapters corresponds 
to one of these papers. 

In this chapter we will first introduce basic notations,
then give some motivations for our investigations
and finish with a short description of the obtained results. 

We denote by $\N$ the set of positive integer numbers and 
we set $\N_0 := \N \cup \{0\}$. 

\section{Young diagrams and Young tableaux}
\label{sec:diagrams-intro}

\emph{A partition of a natural number $n$} 
is a sequence $\lambda = (\lambda_0, \dots, \lambda_k)$ 
(for some $k \in \N_0$)
of natural numbers 
such that $\lambda_0 \geq \lambda_1 \geq \dots \geq \lambda_k > 0$ 
and $n = \sum_{n=0}^k \lambda_n$.  
Sometimes it may be more convenient to think 
of a partition $\lambda$ as an infinite sequence 
$\lambda = (\lambda_p)_{p \in \N_0}$
in which $\lambda_p = 0$ for $p > k$.

With a partition $\lambda$ of a natural number $n$
we associate its graphical representation called \emph{Young diagram}
and denoted also by $\lambda$.
The Young diagram $\lambda = (\lambda_0, \dots, \lambda_k)$
is a figure on the plane  
which consists of $n = \lambda_0 + \dots + \lambda_k$ squares of side $1$
placed one next to the other in $k+1$ rows in such a way that in the $i$-th row 
there are exactly $\lambda_i$ squares.
We use the, so called, \emph{French convention} to draw Young diagrams, see \cref{fig:diagram-a}.
We enumerate the rows and columns of a Young diagram
with numbers from the set $\N_0$ starting with $0$.
%In particular, if $\Box$ is a box of a diagram, we
%identify it with the Cartesian coordinates of its \emph{lower-left corner}, i.e.,
%$\Box=(x,y)\in \N_0\times \N_0$. 
Each of the squares in a Young diagram will be called \emph{a box}.
The number of boxes in the Young diagram $\lambda$ is called 
\emph{the size of $\lambda$}
and denoted by $|\lambda|$.
We also define \emph{the empty diagram}, which we denote by $\emptyset$,
as the diagram which has no boxes.
The set of all Young diagrams with $n$ boxes will be denoted by~$\diagrams_n$
and the set of all Young diagrams by~$\diagrams$, i.e., 
\[
\displaystyle \diagrams = \bigcup_{n=0}^\infty \diagrams_n.
\]
The~set $\diagrams$ has a~structure of an~oriented graph, called \emph{Young graph},
in which an oriented edge from diagram $\mu$ to diagram $\lambda$ is present if 
$\lambda$ can~be created from $\mu$ by addition of a~single box
(in such case we write $\mu\nearrow\lambda$). 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subfile{Chapters/intro/figures-intro/young-diagram-tableau.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For a Young diagram $\lambda$ 
we consider its fillings with $|\lambda|$ real numbers 
with the following properties: 
\begin{itemize}
\item in each row the numbers are increasing from left to right,
\item in each column the numbers are increasing from bottom to top.
\end{itemize}
Any such filling of $\lambda$ we call 
\emph{a Young tableau of shape $\lambda$} (or shortly \emph{tableau}),
see \cref{fig:diagram-b}.
We refer to the numbers in boxes as \emph{entries} of a tableau.

\emph{A standard Young tableau of shape $\lambda$} (or shortly \emph{standard tableau}) 
is a tableau with entries $1, \dots, |\lambda|$, see \cref{fig:diagram-c}.
The set of standard tableaux of shape $\lambda$ will be denoted by $\tableaux_{\lambda}$. 

Let $T$ be a tableau. 
The shape of $T$ 
we denote by $\sh(T)$. 
The number of boxes in $T$ is called 
\emph{the size of tableau} and denoted by $|T|$ or $|\sh(T)|$. 
We denote by $T_{x,y}$ the entry of tableau $T$ 
which lies in the intersection of the row $y\in\N_0$
and the column $x\in\N_0$, for example,
in \cref{fig:RSKa} we have $T_{0,0} = 16, T_{0,1} = 23, T_{1,0} = 37$ and so on.

We investigate asymptotics of two operations related to tableaux -- 
the Robinson--Schensted--Knuth algorithm and jeu de taquin. 

%%%%%%
\section{Robinson--Schensted--Knuth algorithm}
\label{sec:RSK-intro}

The first operation on tableaux of our interest 
is \emph{the Robinson--Schensted--Knuth algorithm} (shortly~\emph{RSK}).
In fact, we will consider a simplified version of
\emph{the Robinson--Schensted--Knuth algorithm}; 
for this reason we should rather call it
\emph{the Robinson--Schensted algorithm}. 
Nevertheless, we use the first name
because of its well-known acronym \emph{RSK}. 

RSK is an iterative algorithm during which we apply
in each step \emph{the Schensted row insertion}.


\subsection{Schensted row insertion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subfile{Chapters/intro/figures-intro/schensted-step.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\emph{The Schensted row insertion} is an algorithm which takes as an input a
tableau $\tab$ and some number $\letter$. The number $\letter$ is inserted
into the first row (that is,~the bottom row, the row with the index~$0$) of
$\tab$ to the~leftmost box which contains an entry which is strictly bigger
than $\letter$.

In the case when the row contains no entries which
are bigger than~$\letter$, we create in the first row new empty box 
directly to the right of $\sh(T)$ and we fill it with the~number~$\letter$,
and the algorithm terminates. 

If, however, the number $\letter$ is inserted into a box which was not empty,
the~previous content $\letter'$ of the box is \emph{bumped} into the next row. 
This means that the algorithm is
iterated but this time the number $\letter'$ is inserted into the next row to
the leftmost box which contains a number bigger than $\letter'$
(if such box exists). 
We repeat these steps of row insertion and bumping 
until some number needs to be inserted into a new empty box.

This process is illustrated on \cref{fig:RSKb,fig:RSKc}. 
The~\emph{bumping route} consists of the~boxes the~entries of which were
changed by the~action of Schensted insertion, 
including the~last, newly created box, see \cref{fig:RSKc}. 
\emph{The outcome of the Schensted insertion} is defined as 
the~result of the~aforementioned procedure;
it will be denoted by $\tab \leftarrow \letter$. 



%\medskip
%
%Note that this procedure is well defined also in the~setup when $\tab$ is
%an~\emph{infinite} tableau (see \cref{fig:tableauFrench} for an~example), even
%if the~above procedure does not terminate after a~finite number of steps.


\subsection{Robinson--Schensted--Knuth algorithm} 

\emph{The Robinson--Schensted--Knuth algorithm}
(shortly~\emph{RSK}) associates to a finite
sequence $w=(w_1,\dots,w_\ell)$ of real numbers
a~pair of tableaux: 
\emph{the insertion tableau $P(w)$} 
and \emph{the recording tableau~$Q(w)$}
of the same shape,
see \cref{fig:RSK-res}.
		
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subfile{Chapters/intro/figures-intro/rsk-result.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
\emph{The insertion tableau} 
\begin{equation}
    \label{eq:insertion}
    P(w) = \Big( \big( (\emptyset \leftarrow w_1) \leftarrow w_2 \big) 
    \leftarrow  \cdots \Big) \leftarrow w_\ell
\end{equation}
is defined as the result of the iterative Schensted row insertion applied to the
entries of the sequence $w$, starting from \emph{the empty tableau $\emptyset$}.

\emph{The recording tableau $Q(w)$} is defined as the standard Young tableau of the
same shape as $P(w)$ in which each entry is equal to the number of 
the~iteration of \eqref{eq:insertion} in which the given box of~$P(w)$ stopped
being empty; in other words the entries of $Q(w)$ give the order in which the
entries of the~insertion tableau were filled.

The common shape of the insertion tableau $P(w)$ and the recording tableau $Q(w)$
will be denoted by $\RSK(w)$.

\begin{theorem}[{\cite[Part~1, Chapter~4]{Fulton1997}}]
\label{thm:fulton-rsk}
For any $n\in \N$ the Robinson--Schensted algorithm 
gives a bijection 
between the symmetric group $\Sym{n}$ and 
the set of pairs $(P,Q)$ of standard tableaux with $n$ boxes
and the same shape.
\end{theorem}

%It is known that the shape of insertion tableau (or, equivalently, recording tableau)
%obtained by applying the RSK algorithm 
%to an i.i.d. $U(0,1)$ sequence 
%is a random Plancherel distributed diagram (see \cref{ex:left-regular} for the definition) 

The RSK algorithm is of great importance in algebraic combinatorics, 
especially in the context of the representation theory \cite{Fulton1997}. 
Also a~fruitful area of study concerns
the RSK algorithm applied to a uniformly random permutation from $\Sym{n}$,
especially asymptotically in the limit $n\to\infty$, 
see \cite{Romik2015a} and the references therein.




%
\section{Jeu de taquin algorithm}
\label{sec:jdt-intro}

The second operation on tableaux whose asymptotics we will investigate 
is \emph{jeu de taquin}, \cite[Section~1.2]{Fulton1997}.
This operation is also heavily used in the~study of Young tableaux. 


\subsection{Jeu de taquin and sliding path}

\emph{Jeu de taquin} acts on Young tableaux in the following way
(see~\cref{fig:jdtA-intro,fig:jdtB-intro}): we remove the bottom-left box of the given
tableau $\tab$ and obtain a~\emph{hole} in its place. Then we look at the two
boxes: the one to the~right and the one above the~hole, and choose the one
which contains the smaller number. We slide this smaller box into the location
of the hole, see \cref{fig:jdtZ-intro}. As a result, the hole moves in the opposite
direction. We continue this operation as long as there is some box to the right or
above the hole. The path which was traversed by the `traveling hole' will be 
called the~\textbf{\emph{\jdtp}}, see~\cref{fig:jdtA-intro}.
The result of \jdtincomplete
applied to a~tableau~$\tab$ will be denoted by~$j(\tab)$, 
see \cref{fig:jdtB-intro}.
Note that the tableau $j(T)$ has one box less than $T$. 

\begin{figure}
    \subfloat[]{
        \begin{tikzpicture}[scale=1.2]
        \clip (-0.3,-0.3) rectangle (2.3,2.3);
        \draw[black!20] (-1,-1) grid (3,3);
        \draw[pattern color=red!50,pattern=north east lines] (0,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[pattern color=green!50,pattern=north west lines] (1,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (1,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (1,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (0,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (0,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (1,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw (0.5,1.5) node [circle,inner sep=2pt,fill=white] {$r$};
        \draw (1.5,0.5) node [circle,inner sep=2pt,fill=white] {$s$};
        \end{tikzpicture}
        \label{subsig:jdtA-intro}}
    \hfill
    \subfloat[]{
        \begin{tikzpicture}[scale=1.2]
        \clip (-0.3,-0.3) rectangle (2.3,2.3);
        \draw[black!20] (-1,-1) grid (3,3);
        \draw[pattern color=red!50,pattern=north east lines] (0,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[pattern color=green!50,pattern=north west lines] (1,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (1,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (1,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (0,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (0,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (1,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw (0.5,0.5) node [circle,inner sep=2pt,fill=white] {$r$};
        \draw (1.5,0.5) node [circle,inner sep=2pt,fill=white] {$s$};
        \draw[ultra thick,->] (0.5,1.5) -- (0.5,0.7);
        \end{tikzpicture}
        \label{subsig:jdtB-intro}}
    \hfill
    \subfloat[]{
        \begin{tikzpicture}[scale=1.2]
        \clip (-0.3,-0.3) rectangle (2.3,2.3);
        \draw[black!20] (-1,-1) grid (3,3);
        \draw[pattern color=red!50,pattern=north east lines] (0,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[pattern color=green!50,pattern=north west lines] (0,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (1,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (1,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (0,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,0) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (0,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (1,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (-1,-1) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw[fill=black!5] (2,2) +(0.1,0.1) rectangle +(0.9,0.9);
        \draw (0.5,1.5) node[circle,inner sep=2pt,fill=white] {$r$};
        \draw (0.5,0.5) node[circle,inner sep=2pt,fill=white] {$s$};
        \draw[ultra thick,->] (1.5,0.5) -- (0.7,0.5);
        \end{tikzpicture}
        \label{subsig:jdtC-intro}}
    \caption{Elementary step of the \jdtincomplete transformation: 
        \protect\subref{subsig:jdtA-intro} the initial configuration of boxes,
        \protect\subref{subsig:jdtB-intro} the outcome of the slide in the case when $r<s$,
        %(more generally, when $x<_r y$),
        \protect\subref{subsig:jdtC-intro} the outcome of the slide in the case when $s<r$.
 Copyright \textcopyright2014 Society for Industrial and Applied Mathematics. 
	Reprinted from \cite{Sniady2014} with permission.  All rights reserved.    
}
	\label{fig:jdtZ-intro}
\end{figure}


%If $\tab$ is a standard tableau then $j(\tab)$ is no longer standard because the~numbering of its boxes
%starts with $2$; however, if we decrease each entry of $j(\tab)$ by $1$ then it becomes standard. This observation allows us to define the \emph{\jdtt} 
%$\jdtcomplete\colon \tableaux_\lambda\to \tableaux_\lambda$ which is a bijection on the set 
%of standard Young tableaux of any fixed shape $\lambda$. 
%The idea is to
%put once again the~box with the~number~$|\tab|$ to the~aforementioned standardized version of
%the tableau~$j(\tab)$ 
%%(which is obtained by decreasing the entries of $j(\tab)$ by $1$)
%in the place where we removed a box during \jdtincomplete, see~\cref{fig:jdtC}.

\begin{figure}[t]
    \centering
    \subfloat[]{
			\begin{tikzpicture}[scale=0.9]
					\fill[blue!20] (0,0) rectangle +(1,1);
					\fill[blue!20] (0,1) rectangle +(1,1);
					\fill[blue!20] (1,1) rectangle +(1,1);
					\fill[blue!20] (2,1) rectangle +(1,1);
					\fill[blue!20] (2,2) rectangle +(1,1);
					
					\begin{scope}
					\clip[](5,0) -- (5,1) -- (4,1) -- (4,2) -- (3,2) -- (3,3) -- (1,3) -- (1,4) -- (0,4) -- (0,0);
					\draw (0,0) grid (10,10);
					\end{scope}
					\draw[ultra thick] (5,0) -- (5,1) -- (4,1) -- (4,2) -- (3,2) -- (3,3) -- (1,3) -- (1,4) -- (0,4) -- (0,0) -- cycle;
					\draw (0.5,0.5) node {$1$};
					\draw (0.5,1.5) node {$2$};
					\draw (1.5,0.5) node {$3$};
					\draw (2.5,0.5) node {$7$};
					\draw (3.5,0.5) node {$10$};
					\draw (4.5,0.5) node {$13$};
					\draw (1.5,1.5) node {$4$};
					\draw (0.5,2.5) node {$5$};
					\draw (2.5,1.5) node {$6$};
					\draw (1.5,2.5) node {$8$};
					\draw (0.5,3.5) node {$11$};
					\draw (2.5,2.5) node {$9$};
					\draw (3.5, 1.5) node {$12$};
			\end{tikzpicture}        
\label{fig:jdtA-intro}
}
%
\hfill
\subfloat[]{
			\begin{tikzpicture}[scale=0.9]
					\fill[blue!20] (0,0) rectangle +(1,1);
					\fill[blue!20] (0,1) rectangle +(1,1);
					\fill[blue!20] (1,1) rectangle +(1,1);
					\fill[blue!20] (2,1) rectangle +(1,1);
					\fill[blue!10] (2,2) rectangle +(1,1);
                    
					\begin{scope}
					\clip[](5,0) -- (5,1) -- (4,1) -- (4,2) -- (2,2) -- (2,3) -- (1,3) -- (1,4) -- (0,4) -- (0,0);
					\draw (0,0) grid (10,10);
					\end{scope}
					\draw[ultra thick] (5,0) -- (5,1) -- (4,1) -- (4,2) -- (2,2) -- (2,3) -- (1,3) -- (1,4) -- (0,4) -- (0,0) -- cycle;
					\draw (0.5,0.5) node {$2$};
					\draw (0.5,1.5) node {$4$};
					\draw (1.5,0.5) node {$3$};
					\draw (2.5,0.5) node {$7$};
					\draw (3.5,0.5) node {$10$};
					\draw (4.5,0.5) node {$13$};
					\draw (1.5,1.5) node {$6$};
					\draw (0.5,2.5) node {$5$};
					\draw (2.5,1.5) node {$9$};
					\draw (1.5,2.5) node {$8$};
					\draw (0.5,3.5) node {$11$};
					\draw (3.5, 1.5) node {$12$};
			\end{tikzpicture}
\label{fig:jdtB-intro}
}        
\caption{\protect\subref{fig:jdtA-intro} A standard Young tableau
$\tab$ of shape $\lambda = (5,4,2,1)$. The highlighted boxes form the~\emph{\jdtp}.
\protect\subref{fig:jdtB-intro} The outcome $j(\tab)$ of the \jdtincomplete
transformation. The light blue empty square indicates the box 
which got removed during \jdtincomplete.}
\label{fig:jdt-intro}
\end{figure}

%\pagebreak


\subsection{Evacuation path}

For a given \emph{standard tableau} $\tab\in \tableaux_\lambda$ with $n=|\lambda|$ boxes the \jdtincomplete transformation $j$ can be 
iterated $n$ times until we end with the empty tableau. 
During each iteration the box with the biggest number $n$ either moves one node left or down, or stays put. 
Its trajectory 
%\begin{equation}
%\label{eq:evacuationtrajectory}
%\evac(T)=
%\Big( 
%\pos_n(\tab),\ 
%\pos_n\!\big( j(\tab) \big),
%%\ 
%%\pos_n\big( j^2(\tab) \big),
%\ \dots,\
%\pos_n\! \big( j^{n-1}(\tab) \big) 
%\Big) 
%\end{equation}
will be called the~\textbf{\emph{evacuation path}}.



%%%%%%%%%%%
\section{Basics of representation theory}
\label{sec:representation-theory-basics}

Young diagrams and tableaux are connected with 
the irreducible representations of the symmetric groups.
We will now give a very short introduction to the representation theory. 

Let $V$ be a vector space over the field $\K = \R$ or $\K = \C$ 
and denote by $\GL(V)$ the set of isomorphisms of $V$. 
Let $G$ be a finite group
and let $\rho: G \to \GL(V)$ be a homomorphism,
i.e., $\rho(g h) = \rho(g) \circ \rho(h)$ for all $g,h \in G$.
Depending on the context, we call \emph{a representation of $G$} 
the homomorphism $\rho$ or the linear space $V$.
We also call a homomorphism $\rho$ \emph{a linear group action of $G$} on $V$.
We will explain this ambiguity below.
%If $\K = \R$ we call the representation \emph{real} 
%and if $\K = \C$ we call it \emph{complex}. 

%Equivalently, one can define a representation $\rho$ 
%as a linear action of a group $G$ on a vector space $V$,
%i.e., a function $\rho: G \times V \to V$ which fulfills the following properties
%\begin{itemize}
%\item $\rho(g h)(v) = \rho(g) \left( \rho(h)(v) \right)$ for all $g,h \in G$ and $v \in V$;
%\item $\rho(e)(v) = id(v) = v$ for all $v \in V$ (where $e$ denotes the neutral element of $G$);
%\item $\rho(g)(\alpha v + \beta w) = \alpha \rho(g)(v) + \beta\rho(g)(w)$ for all $g \in G$ and $\alpha, \beta \in \K$ and $v,w \in V$.
%\end{itemize}

Let $V$ and $W$ be two vector spaces
and let $\rho: G \to \GL(V)$ and $\pi: G \to \GL(W)$
be their representations.
We say that the mapping $\phi : V \to W$ is a \emph{homomorphism of representations} if
\[
\bigforall_{g \in G}\ \ \  \phi \! \left( \rho(g)(v) \right) = \pi(g) \! \left( \phi(v) \right),
\]
i.e., $\phi$ intertwines the representations $\rho$ and $\pi$.
Additionally, if $\phi$ is a bijection then we call it \emph{isomorphism of representations}.
If such isomorphism exists then we say that the \emph{representations $V$ and $W$ are equivalent} 
(or \emph{isomorphic}).

A subspace $W$ of the vector space $V$ which is invariant under 
the (linear) group action of~$G$
is called \emph{a subrepresentation}. 
We say that a representation $V$ is \emph{irreducible} if it does not have 
nontrivial subrepresentations, i.e., other than $\{0\}$ and $V$. 

\begin{example}[Irreducible representations of the symmetric group]
\label{ex:symmetric-group-irreducibles}
Each irreducible representation of the symmetric group $\Sym{n}$ 
corresponds to some Young diagram with $n$ boxes, \cite[Theorem~2.4.4]{Sagan2001}. 
In particular, the trivial representation corresponds to the Young diagram $(n)$ with $1$ row
and the alternating representation corresponds to the Young diagram $(1^n)$ with $n$ columns.

The basis of the irreducible representation of $\Sym{n}$ 
corresponding to the Young diagram $\lambda \in \Y_n$ 
can be encoded with the set of standard tableaux of shape $\lambda$, 
\cite[Theorem~2.6.4]{Sagan2001}.
The dimension of such irreducible representation is equal to 
the number $d_\lambda$ of standard tableaux of the shape~$\lambda$. 
\end{example}

The following two theorems lie in the foundations of the representation theory. 

\begin{lemma}[Schur's lemma]
Let $V$ and $W$ be irreducible representations of the finite group $G$.
Assume that $T : V \to W$ is a homomorphism of representations. 
Then $T = 0$ or $T$ is an isomorphism.
In particular, the only homomorphisms of irreducible representations of $V$
are the multiples of identity. 
\end{lemma}

\begin{theorem}[Maschke's theorem]
Let $G$ be a finite group.
Any finitely dimensional representation $V$ 
(over the field $K = \R$ or $K = \C$) of $G$
is a direct sum of irreducible representations of $G$, i.e.,
\[
V = \bigoplus_{i=1}^k W_i
\]
where each $W_i$ is an irreducible representation of $G$. 
The decomposition of $V$ into irreducible components is unique up to an isomorphism of representations. 
\end{theorem}

Taking into account Schur's lemma and Maschke's theorem
it becomes clear that the two notions of a representation: 
first as the group homomorphism and second as the vector space 
can be used interchangeably. 
%These two notions are equivalent up to an isomorphism.

\begin{example}[The left regular representation]
\label{ex:left-regular}
The following example is of special interest since 
\begin{itemize}
\item each irreducible representation of $G$ is 
isomorphic to some irreducible subrepresentation of 
the left regular representation; 
\item the celebrated and intensively investigated 
\emph{Plancherel measure} is a very natural probability measure 
on the set of irreducible components of the left regular representation.
%the uniform measure on the irreducible components 
%of the left regular representation is 
%the celebrated 
%which is intensively investigated. 
\end{itemize} 

Let $G$ be a group and let $\K G$ be the group algebra of $G$, i.e.,
$\K G$ is the set of all formal linear combinations of the elements of $G$, i.e.,  
\[
\K G = \left\{ \sum_{g \in G} a_g g: \ a_g \in \K \right\}.
\]
We consider the left action of $G$ on the group algebra $\K G$, i.e.,
the representation $\rho : G \to \GL(\K G)$ given by 
\[
\bigforall_{h \in G}\ \ \  
\rho(h)\! \left( \sum_{g \in G} a_g g \right) := \sum_{g \in G} a_g hg.
\] 

By Maschke's theorem $\K G$ decomposes into the direct sum of the irreducible components.
Using the theory of characters one can show that some of these irreducible components
are isomorphic and $\K G$ can be given as the following direct sum 
\[
\K G = \bigoplus_{\nu} c_\nu V_\nu
\]
where each component is indexed by different conjugation class $\nu$ of $G$
and the coefficient $c_\nu \in \N$, called \emph{multiplicity of $V_\nu$}, 
is equal to the cardinality of $\nu$.
Moreover, $c_\nu$ is equal to the dimension of 
the vector space $V_\nu$, i.e.,
$c_\nu  = |\nu| = \on{dim} V_\nu$. 

The probability measure on the set of all 
irreducible subrepresentations of $\K G$ 
given by the formula 
\[
\Plancherel(\nu) := \frac{\on{dim} V_\nu^2}{|G|}, \quad \textrm{$\nu$ -- irreducible component of $\K G$},
\]
is called \emph{the Plancherel measure}.
\end{example}

The correspondence between irreducible representations of $\Sym{n}$
and Young diagrams, cf.~\cref{ex:symmetric-group-irreducibles},
yields \emph{the Plancherel distribution} on Young diagrams with $n$ boxes.
The probability of choosing the diagram $\lambda \in \diagrams_n$
with respect to the Plancherel measure on $\Sym{n}$ is equal to
\begin{equation}
\label{eq:prob-plancherel-diagram}
\Plancherel_n(\lambda) = \frac{(d_\lambda)^2}{n!}
\end{equation}
where $d_\lambda$ denotes the number of standard tableaux of shape $\lambda$.
The Plancherel measure $\Plancherel_n$ plays a crucial role in our research,
%in the case when $G = \Sym{n}$ and $n \to \infty$,
see \cref{ch:poisson,ch:bumping}.
%It is well known that the common shape of the insertion and the recording tableaux
%obtained by an application of the RSK algorithm
%to the i.i.d. $U(0,1)$ sequence of random variables
%is a \emph{


%%%%%%%%%%%
\section{Motivations for the results}
\label{sec:motivations-intro}

Many combinatorial structures can be viewed as \emph{discrete versions 
of continuous geometric objects}. 
For example, Young diagram is a geometric shape on the Cartesian plane
and a standard tableau can be viewed as a 3D block
being a union of `small' cuboids with the square base $1 \times 1$ 
corresponding to the size of a cell in the Young diagram
(the shape of tableau)
and the height equal to the entry in the corresponding cell.  
We can embed these structures into another space of 
some (continuous) geometric objects 
and ask whether there is some \emph{typical asymptotic behaviour 
of these discrete shapes as their size parameter tends to infinity}. 
In many cases the randomly sampled element approaches some continuous limit.
If this is a case we say that the model has \emph{a limit shape}.
\cref{fig:VKLS-curve} shows an example of the limit shape phenomenon
for the Plancherel distributed random diagram.
The questions concerning the limit shapes 
are usually formulated \emph{in the probabilistic setup}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subfile{Chapters/intro/figures-intro/VKLS-curve.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%In particular, \emph{the convergence to the limit shape} 
%should be understood in some sense provided by the probability theory.

It is worth noting that the investigation of the limit shapes of 
random combinatorial objects
is very appealing to wide range of mathematicians.
Such problems were investigated, for example, by 
a Fields medal laureate Andrei Okounkov \cite{Okounkov2006},
Richard Stanley \cite{Stanley2007} and Anatoly Vershik \cite{Vershik1995}.
Also the research in this area was appreciated by the mathematical community 
by invitations to give lectures on International Congress of Mathematics by Philippe Biane \cite{Biane2002}
or European Congress of Mathematics by my supervisor Piotr \'Sniady \cite{Sniady2013}.

There are several good reasons for studying limit shapes:
\begin{itemize}
\item Random combinatorial objects can be often considered as models of mathematical physics.
We raise this issue in the case of \emph{Totally Asymmetric Simple Exclusion Process} 
(shortly \emph{TASEP}) in \cref{ch:evacuation}.
\item On one hand, the representation theory is often 
related to some combinatorial structures 
(see \cref{ex:symmetric-group-irreducibles} for an example).
On the other hand, questions from group theory, 
harmonic analysis on groups, probability on groups 
or quantum information theory can be often rephrased in the language 
of the character theory.
Therefore it is essential to understand large combinatorial objects 
to answer the original question.
\item The \emph{aesthetical motivation} which is twofold.
Firstly, the computer simulations often gives rise to beautiful pictures.
Secondly, and more importantly, the solutions of the problems 
related to asymptotics of random combinatorial structures 
often involve an appealing interface between seemingly distant disciplines of mathematics,
such as combinatorics, analysis, harmonic analysis, ergodic theory, representation theory, 
probability theory or quantum mechanics.
\end{itemize}

In this thesis we are going to investigate 
\emph{the dynamics of the growth of 
the first few bottom rows of a Young diagram} 
during the RSK insertion applied to a random input (\cref{ch:poisson}),
\emph{the limit shape of the bumping route} corresponding to 
the Schensted insertion of `very small' numbers (\cref{ch:bumping})
and \emph{the limit shapes of the \jdtp and evacuation path} 
in a random tableau of rectangular shape 
and \emph{the dynamics of a particular TASEP process} (\cref{ch:evacuation}). 
We will describe these specific problems and their motivations 
more richly in the corresponding chapters. 



%%%%%%%%%%%
\section{Content of the thesis}
\label{sec:content-intro}

The proper part of this PhD thesis consists of three (almost) independent chapters.

In the first of these chapters, \cref{ch:poisson}, 
we investigate \emph{the way 
in which the first $k$ bottom rows grow 
in a random Plancherel distributed diagram}. 
To be more precise, we consider an i.i.d. $U(0,1)$ infinite word $w = (w_1, w_2, \dots)$
and apply iteratively the RSK algorithm to its restrictions $w|_n := (w_1, \dots, w_n), n \in \N$,
and look at the obtained Young diagrams $\RSK(w|_n)$. 
%shapes of the obtained recording tableaux~$Q(w|_n)$. 
Each two results of the consecutive iteration steps 
differ by exactly one box (which was added in some row). 
%In each iteration step one new box is added in some row. 
Our aim is to show that the growths which happen in the first $k$ bottom rows 
are asymptotically independent and that asymptotically the growth
in each of them in time $n$ can be modelled by a Poisson process with intensity $n^{-1/2}$.

In the next chapter, \cref{ch:bumping}, we use the obtained result from \cref{ch:poisson}
to investigate \emph{the bumping routes in the surrounding of the first column of random Young tableau}. 
More precisely, to a big random Plancherel distributed standard tableau 
we Schensted-insert a number $m + \frac{1}{2}$ for some fixed $m \in \N$ 
and look at the corresponding bumping route. 
We are interested in the rows in which the bumping route `jumps between' the columns. 
We show that these rows (when properly seen) can be asymptotically modelled by a Poisson process.

The last chapter, \cref{ch:evacuation}, is devoted to 
\emph{the typical shapes of the \jdtp and the evacuation path}
in a random rectangular standard tableau.
We show that each of the investigated random paths asymptotically focuses
near a random curve belonging to some particular family.
The tools used in the proofs can be applied in a more general context
of $C$-balanced tableaux. 
We then transfer these results to the setup of \emph{Totally Asymmetric 
Simple Exclusion Process} to obtain the description of
\emph{the limit trajectory of the second class particle} in TASEP. 