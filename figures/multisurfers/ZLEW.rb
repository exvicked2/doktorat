#!/usr/bin/env ruby


require 'erb'

Infinity=10000000

class Letter 
 
    include Comparable

    def value
        @value
    end  


    def <=>(other)
        return value<=>other.value 
    end  


    def initialize(params)
          @value=params[:value]
    end    

    
    def set_value(new_value)
        @value=new_value
    end
      
    def special?
        return @special
    end
    
end


def special_color(wartosc)
    case wartosc
            when 97 then "red!20"
            when 98 then "red!40"
            when 99 then "red!60"
            when 100 then "red!80"
    end
end


class MyLetter < Letter
    
    def initialize(params)
        @value=params[:value]
        if @value>=97 then 
            @special=true
        else
            @special=false
        end
    end
           

end




class Array
  

    def jdt
        x=0
        y=0
#        wywalone=self[0][0]
#        wywalone.set_value(wywalone.value+1000000)
        
        
#        ta pętla działa tak długo, jak jest coś u gory oraz na prawo
        loop do
#            CZY DA SIĘ IŚĆ W PRAWO?
#            ostatnia klatka ma numer length-1
            break if x>= self[y].length-1
            
#            CZY DA SIĘ IŚĆ DO GÓRY?
#            ostatni wiersz ma numer length-1
            break if y>=self.length-1
            break if x>=self[y+1].length
            
            if self[y][x+1]<self[y+1][x]
                then nx,ny=x+1,y
                else nx,ny=x,y+1
                end
             
            box=self[ny][nx]
            if box.special? then $animator.special_paths[box.value] << [nx,ny] end

                
            self[y][x]=self[ny][nx]
                
            x,y=nx,ny
        end
        
        
        loop do
            break if x>=self[y].length-1
#            coś jest na prawo. więc nie ma nic u gory
            nx,ny=x+1,y
            
            box=self[ny][nx]
            if box.special? then $animator.special_paths[box.value] << [nx,ny] end

            self[y][x]=self[ny][nx]
            x,y=nx,ny
        end
            
  
        loop do
            
#            ostatni wiersz ma numer length-1
            break if y>=self.length-1
            break if x>=self[y+1].length
            
            nx,ny=x,y+1       
            box=self[ny][nx]
            if box.special? then $animator.special_paths[box.value] << [nx,ny] end
            self[y][x]=self[ny][nx]
            
            x,y=nx,ny
        end

            
#        self[y][x]= wywalone   
            self[y].pop
    end



end



    

    
   

    
    
    
    
    
class Animator
  
  
  def initialize
    @tableau=StartTableau.collect{|line| line.collect{|value| MyLetter.new(:value => value,)}}
    @frames=Array.new
    @time=0
    @special_paths={97=>[], 98=>[], 99=>[],  100=>[]} 
  end

    
    def set_time(t)
        @time=t
    end

  
  def frames
    @frames
  end
  
    
  def tableau
    @tableau
  end  
    
    
    def special_paths
        @special_paths
    end
    
    
  
  def snapshot(args={})

        rysunek='\only<+>{  '
        rysunek<< "\n"
        rysunek<< 
        "\\begin{tikzpicture}[scale=0.7]
\\clip (-0.5,-0.5) rectangle (10.5,10.5);
"

#        @special_paths.each do |value,path|
#            path.each do |x,y|
#                rysunek << "\\fill[#{special_color(value)},opacity=0.4] (#{x},#{y}) rectangle +(1,1); \n"
#            end
#        end
      
        rysunek << "\\draw[black!10] (0,0) grid (10,10);
"

      
        @tableau.each_with_index do |line,y|
                    line.each_with_index do |letter,x|
                            if not(letter.special?) then
                                rysunek << "\\draw[black,fill=blue!10] (#{x},#{y}) rectangle +(1,1); \n"
                                rysunek << "\\draw[black] (#{x+0.5},#{y+0.5}) node {#{args[:fontsize]} $ #{letter.value} $};\n"
                            else
                                
                                kolor=special_color(letter.value)
                                        
                                rysunek << "\\fill[fill=#{kolor}] (#{x},#{y}) rectangle +(1,1); \n"
                                rysunek << "\\draw[white] (#{x+0.5},#{y+0.5}) node {$ #{letter.value} $};\n"
                            end
                                 
                        end
              end    

            
        diagram= @tableau.collect{|line| line.length}
            
            
        wklesle_rogi=([Infinity] + diagram << 0).each_cons(2).each_with_index.select {|pair,row| a,b=pair; a!=b}.map{|pair,row| [pair.last,row]} 

  
        wypukle_rogi=([Infinity] + diagram << 0).each_cons(2).each_with_index.select {|pair,row| a,b=pair; a!=b}.map{|pair,row| [pair.first,row]}  
    
            
        punkty=[wypukle_rogi,wklesle_rogi].transpose.flatten(1)[1..-1]            
        
        rysunek << "\\draw[thick]" << punkty.collect { |x,y| "(#{x},#{y})" }.join(" -- ") << "; \n"       
            
            
        rysunek << " 
\\draw[ultra thick] (0,0) rectangle (10,10); 
\\end{tikzpicture} }\n\n"
        
        @frames << rysunek
  end  
  
end
  

    
        
    
        

def animacja
    
        $animator=Animator.new
        
        $animator.snapshot
    
        (1..100-3).each do |t|
                $animator.snapshot(fontsize:'\small') 
                $animator.tableau.jdt
          end


        File.open('zlew.tex', 'w') {|f| f.write($animator.frames.join("\n\n")) }

end
    
    
    
StartTableau= [[1, 3, 4, 6, 11, 18, 21, 23, 52, 68], [2, 5, 7, 9, 16, 20, 24, 40, 59, 71], [8, 12, 26, 27, 34, 35, 37, 44, 62, 80], [10, 13, 28, 31, 36, 42, 43, 63, 73, 82], [14, 15, 32, 38, 41, 45, 58, 67, 78, 87], [17, 22, 33, 39, 47, 50, 66, 74, 85, 88], [19, 25, 48, 49, 53, 69, 76, 79, 86, 92], [29, 46, 55, 56, 60, 70, 77, 89, 91, 93], [30, 51, 57, 61, 65, 72, 83, 90, 95, 96], [54, 64, 75, 81, 84, 94,    97,98,99,100]]
    
animacja
    
    
    

    
