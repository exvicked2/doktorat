#!/usr/bin/env ruby

require './young'
Konwencja=:francuska





class DiagramYounga


# wielokat przedstawiajacy obrys diagramu Younga
  def obrys_skrocony(opcje="")      
      punkty=[wypukle_rogi,wklesle_rogi].transpose.flatten(1)[1..-1];
                 
      punkty= punkty - [ [Size,0] , [0,Size] ]
      
      linia(punkty,opcje)
  end

end



def element(tableau,column,row)
    (tableau[row] or [])[column]
end



def jeu_de_taquin(tableau, type=:cyclic)
   #tableau=tableau.collect{|l| l.dup}
   minimum=element(tableau,0,0)
   trajektoria=[]
   i=0
   j=0

   loop do
        trajektoria << [i,j]
        break if element(tableau,i,j+1).nil? and element(tableau,i+1,j).nil?
        #  działa dla SemiStandard tableax.
        if (element(tableau,i,j+1) or Infinity) < (element(tableau,i+1,j) or Infinity) 
                           then i1,j1=[i,j+1] else i1,j1=[i+1,j] end
        tableau[j][i]=element(tableau,i1,j1)
        i,j=[i1,j1]
   end
       
   if type==:cyclic then tableau[j][i]=minimum+4*Size*Size
                    else tableau[j].pop end

   trajektoria 
end







                                   
          

Size=30;
       
a=Size
diagram= DiagramKerova.new([[-2*a,0,3*a],[-a,2*a]])

       
kolory=["Set1-9-1","Set1-9-2","Set1-9-3","Set1-9-4","Set1-9-5","Set1-9-6","Set1-9-7","Set1-9-8","Set1-9-9",]
           
       


       
       
tableau    = diagram.losowe_tableau
       
       
trajektorie= (1..9).collect { jeu_de_taquin(tableau, :cyclic) }
       

grafika = '
\documentclass[minimal.tex]{subfiles}
\begin{document} 
\centering
\begin{tikzpicture}[scale=0.08]
\definecolor{Set1-9-1}{RGB}{228,26,28}
\definecolor{Set1-9-A}{RGB}{228,26,28}
\definecolor{Set1-9-2}{RGB}{55,126,184}
\definecolor{Set1-9-B}{RGB}{55,126,184}
\definecolor{Set1-9-3}{RGB}{77,175,74}
\definecolor{Set1-9-C}{RGB}{77,175,74}
\definecolor{Set1-9-4}{RGB}{152,78,163}
\definecolor{Set1-9-D}{RGB}{152,78,163}
\definecolor{Set1-9-5}{RGB}{255,127,0}
\definecolor{Set1-9-E}{RGB}{255,127,0}
\definecolor{Set1-9-6}{RGB}{255,255,51}
\definecolor{Set1-9-F}{RGB}{255,255,51}
\definecolor{Set1-9-7}{RGB}{166,86,40}
\definecolor{Set1-9-G}{RGB}{166,86,40}
\definecolor{Set1-9-8}{RGB}{247,129,191}
\definecolor{Set1-9-H}{RGB}{247,129,191}
\definecolor{Set1-9-9}{RGB}{153,153,153}
\definecolor{Set1-9-I}{RGB}{153,153,153}'
       

grafika << wielokat([FrenchCoordinates.new([0,0]), FrenchCoordinates.new([0,2*Size]), FrenchCoordinates.new([Size,2*Size]), FrenchCoordinates.new([Size,Size]), FrenchCoordinates.new([3*Size,Size]), FrenchCoordinates.new([3*Size,0]) ], opcje="ultra thick" );
     

trajektorie.each_with_index do |trajektoria, indeks|
       trajektoria.each do |x,y| 
           grafika << "\\fill[draw=#{kolory[indeks]},fill=#{kolory[indeks]},fill opacity=0.6] (#{x},#{y}) rectangle +(1,1); \n"
       end
   end
  
  
       
grafika << '\end{tikzpicture}
\end{document}
'
    
    
puts grafika
       
       
       
#       opis= "\\tiny" << tableau.kolorowane_tableau_na_szaro(:nocontent=>true)
## :nocontent=>true)
#puts opis.latex_file("scale=#{8.0 /a}")