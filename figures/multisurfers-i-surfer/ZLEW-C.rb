#!/usr/bin/env ruby


require 'erb'

Infinity=10000000

class Letter 
 
    include Comparable

    def value
        @value
    end  


    def <=>(other)
        return value<=>other.value 
    end  


    def initialize(params)
          @value=params[:value]
    end    

    
    def set_value(new_value)
        @value=new_value
    end
      
    def special?
        return @special
    end
    
end


def special_color(wartosc)
    case wartosc
            when 51 then "MultiA"
            when 52 then "MultiB"
            when 53 then "MultiC"
            when 54 then "MultiD"
    end
end


class MyLetterSingle < Letter
    
    def initialize(params)
        @value=params[:value]
        if @value==51 then 
            @special=true
        else
            @special=false
        end
    end           
        
end

    
class MyLetterMulti < Letter
    
    def initialize(params)
        @value=params[:value]
        if @value>=51 then 
            @special=true
        else
            @special=false
        end
    end           
        
end

    



class Array
  

    def jdt(animator)
        x=0
        y=0
#        wywalone=self[0][0]
#        wywalone.set_value(wywalone.value+1000000)
        
        
#        ta pętla działa tak długo, jak jest coś u gory oraz na prawo
        loop do
#            CZY DA SIĘ IŚĆ W PRAWO?
#            ostatnia klatka ma numer length-1
            break if x>= self[y].length-1
            
#            CZY DA SIĘ IŚĆ DO GÓRY?
#            ostatni wiersz ma numer length-1
            break if y>=self.length-1
            break if x>=self[y+1].length
            
            if self[y][x+1]<self[y+1][x]
                then nx,ny=x+1,y
                else nx,ny=x,y+1
                end
             
            box=self[ny][nx]
            if box.special? then animator.special_paths[box.value] << [nx,ny] end

                
            self[y][x]=self[ny][nx]
                
            x,y=nx,ny
        end
        
        
        loop do
            break if x>=self[y].length-1
#            coś jest na prawo. więc nie ma nic u gory
            nx,ny=x+1,y
            
            box=self[ny][nx]
            if box.special? then animator.special_paths[box.value] << [nx,ny] end

            self[y][x]=self[ny][nx]
            x,y=nx,ny
        end
            
  
        loop do
            
#            ostatni wiersz ma numer length-1
            break if y>=self.length-1
            break if x>=self[y+1].length
            
            nx,ny=x,y+1       
            box=self[ny][nx]
            if box.special? then animator.special_paths[box.value] << [nx,ny] end
            self[y][x]=self[ny][nx]
            
            x,y=nx,ny
        end

            
#        self[y][x]= wywalone   
            self[y].pop
    end



end



    

    
    
    ###############################################################
    ###############################################################
    ###############################################################
    ###############################################################
    ###############################################################
    
class Animator
  
  
  def initialize
    @tableau=$StartTableau.collect{|line| line.collect{|value| MyLetterSingle.new(:value => value,)}}
    @frames=Array.new
    @time=0
    @speial_paths={}  
  end

    attr_accessor :frames, :tableau, :special_paths    
  
    
    
    
    def snapshot(args={})

       rysunek = ""
      
        @tableau.each_with_index do |line,y|
                line.each_with_index do |letter,x|
                    rysunek << "\\draw[black,fill=blue!10] (#{x},#{y}) rectangle +(1,1); \n"
                    rysunek << "\\draw[black] (#{x+0.5},#{y+0.5}) node {#{args[:fontsize]} $ #{letter.value} $};\n"
                                 
                        end
              end    

            
        diagram= @tableau.collect{|line| line.length}
            
            
        wklesle_rogi=([Infinity] + diagram << 0).each_cons(2).each_with_index.select {|pair,row| a,b=pair; a!=b}.map{|pair,row| [pair.last,row]} 

  
        wypukle_rogi=([Infinity] + diagram << 0).each_cons(2).each_with_index.select {|pair,row| a,b=pair; a!=b}.map{|pair,row| [pair.first,row]}  
    
            
        punkty=[wypukle_rogi,wklesle_rogi].transpose.flatten(1)[1..-1]            
        
        rysunek << "\\draw[thick]" << punkty.collect { |x,y| "(#{x},#{y})" }.join(" -- ") << "; \n"       
            
            
   
        
        return rysunek
  end  
  
end
  

    
    
        ###############################################################
    ###############################################################
    ###############################################################
    ###############################################################
    ###############################################################

    
    
class AnimatorMulti
  
  
  def initialize
    @tableau=$StartTableauMulti.collect{|line| line.collect{|value| MyLetterMulti.new(:value => value,)}}
    @frames=Array.new
    @time=0
#    @speial_paths={}  
    @special_paths={51=>[], 52=>[], 53=>[],  54=>[]} 
  end

    attr_accessor :frames, :tableau, :special_paths    
  
    
    
    
    def snapshot(args={})
      
        rysunek = ""
        
        @tableau.each_with_index do |line,y|
                    line.each_with_index do |letter,x|
                            if letter.special? then  
                                kolor=special_color(letter.value)
                                        
                                rysunek << "\\fill[fill=#{kolor}] (#{x},#{y}) rectangle +(1,1); \n"
#                                rysunek << "\\draw[white] (#{x+0.5},#{y+0.5}) node {$ #{letter.value-46} $};\n"
                            end
                                 
                        end
              end    

            
            
        return rysunek
  end  
  
end
  

    
                ###############################################################
    ###############################################################
    ###############################################################
    ###############################################################
    ###############################################################

    
class AnimatorSingle
  
  
  def initialize
    @tableau=$StartTableauSingle.collect{|line| line.collect{|value| MyLetterSingle.new(:value => value,)}}
    @frames=Array.new
    @time=0
    @special_paths={51=>[]} 
  end

    attr_accessor :frames, :tableau, :special_paths    
  
    
    
    
    def snapshot(args={})

        rysunek=""
        
#        @special_paths.each do |value,path|
#            path.each do |x,y|
#                rysunek << "\\fill[#{special_color(value)},opacity=0.4] (#{x},#{y}) rectangle +(1,1); \n"
#            end
#        end
      
        
      
        @tableau.each_with_index do |line,y|
                    line.each_with_index do |letter,x|
                            if letter.special? then
                                rysunek << "\\draw[ultra thick,red] (#{x},#{y}) rectangle +(1,1); \n"
                                rysunek << "\\draw[red] (#{x+0.5},#{y+0.5}) node {$ X $};\n"
                            end
                                 
                        end
              end    

        
        return rysunek
  end  
  
end
  

    
        
    
        
    
        

def animacja
    
       wypluwka = ""
    
$StartTableau= [
[38], 
[29, 48], 
[26, 44], 
[24, 35, 49], 
[23, 31, 43, 47], 
[18, 19, 30, 37, 40, 45], 
[ 8, 11, 15, 28, 33, 42], 
[ 5,  9, 14, 17, 20, 27, 39, 50], 
[ 2,  7, 10, 12, 16, 25, 32, 36, 46], 
[ 1,  3,  4,  6, 13, 21, 22, 34, 41]].reverse

    
$StartTableauMulti= [
[38, 51], 
[29, 48], 
[26, 44], 
[24, 35, 49], 
[23, 31, 43, 47], 
[18, 19, 30, 37, 40, 45], 
[ 8, 11, 15, 28, 33, 42, 52, 53], 
[ 5,  9, 14, 17, 20, 27, 39, 50], 
[ 2,  7, 10, 12, 16, 25, 32, 36, 46], 
[ 1,  3,  4,  6, 13, 21, 22, 34, 41, 54]].reverse    
    
    
$StartTableauSingle= [
[38], 
[29, 48], 
[26, 44], 
[24, 35, 49], 
[23, 31, 43, 47], 
[18, 19, 30, 37, 40, 45], 
[ 8, 11, 15, 28, 33, 42, 51], 
[ 5,  9, 14, 17, 20, 27, 39, 50], 
[ 2,  7, 10, 12, 16, 25, 32, 36, 46], 
[ 1,  3,  4,  6, 13, 21, 22, 34, 41]].reverse

    
        $animator      =Animator.new
        $animatorMulti =AnimatorMulti.new
        $animatorSingle=AnimatorSingle.new
        
    frames=[]
    
    
    frames <<  ""
    frames.last << "\\only<+>{  
        \\begin{tikzpicture}[scale=0.6]
\\clip (-0.5,-0.5) rectangle (10.5,10.5);
\\draw[black!10] (0,0) grid (10,10);
"

  
    frames.last << $animator.snapshot
    frames.last <<   " 
\\draw[ultra thick] (0,0) rectangle (10,10); 
\\end{tikzpicture} }\n\n"

  
    
  frames <<  ""
    frames.last << "\\only<+>{  
        \\begin{tikzpicture}[scale=0.6]
\\clip (-0.5,-0.5) rectangle (10.5,10.5);
\\draw[black!10] (0,0) grid (10,10);
"

    frames.last << $animator.snapshot
    frames.last << $animatorSingle.snapshot
    frames.last <<   " 
\\draw[ultra thick] (0,0) rectangle (10,10); 
\\end{tikzpicture} }\n\n"

    

    
    frames <<  ""
    frames.last << "\\only<+>{  
        \\begin{tikzpicture}[scale=0.6]
\\clip (-0.5,-0.5) rectangle (10.5,10.5);
\\draw[black!10] (0,0) grid (10,10);
"

    frames.last << $animatorMulti.snapshot
    frames.last << $animator.snapshot
    frames.last << $animatorSingle.snapshot
    frames.last <<   " 
\\draw[ultra thick] (0,0) rectangle (10,10); 
\\end{tikzpicture} }\n\n"

    
    50.times do
        $animator.tableau.jdt($animator)
        $animatorMulti.tableau.jdt($animatorMulti)
        $animatorSingle.tableau.jdt($animatorSingle)
        
        frames <<  ""
        frames.last << "\\only<+>{  
        \\begin{tikzpicture}[scale=0.6]
\\clip (-0.5,-0.5) rectangle (10.5,10.5);
\\draw[black!10] (0,0) grid (10,10);
"

        frames.last << $animatorMulti.snapshot
        frames.last << $animator.snapshot
        frames.last << $animatorSingle.snapshot
        frames.last <<   " 
\\draw[ultra thick] (0,0) rectangle (10,10); 
\\end{tikzpicture} }\n\n"

    end
    
        
    
    wypluwka << frames.join("\n\n")    
    
    
    
    File.open('zlew-C.tex', 'w') {|f| f.write(wypluwka) }

end
    
    
    
    
animacja
    
    
    

    
