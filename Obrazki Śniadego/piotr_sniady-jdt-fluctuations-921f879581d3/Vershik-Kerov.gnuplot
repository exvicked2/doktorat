set format "% .5f"
set samples 100
set parametric

set dummy z


### Vershik Kerov curve
VK(s)=(2/pi)*( s*asin(s/2) + sqrt(4-s**2) )


zn(z)=z
tn(z)=VK(z)


xx(z)=(zn(z)+tn(z))/2
yy(z)=(-zn(z)+tn(z))/2


set trange [-2:2]

set table "vershik-kerov-FR.txt"
plot xx(z),yy(z)

set table "vershik-kerov-RU.txt"
plot zn(z),tn(z)







