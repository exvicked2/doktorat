#!/usr/bin/env ruby


require 'erb'


####################################

class String

def latex_file(opcje) 



template = %q{
\documentclass[12pt]{amsart}
\usepackage{amsmath}

\usepackage{eucal}
\usepackage{times}
\usepackage{euler}
\usepackage{eufrak}

\usepackage{tikz}

\begin{document}

\begin{figure}[htbp]
\tiny
<%= Seed %>
\begin{tikzpicture}[<%= opcje[:tikzpicture] %>]
<%=   self    %> \end{tikzpicture}
\caption{Blabla}
\end{figure}
\end{document}

}

    rtex = ERB.new(template, 0, "%<>");

    rtex.result(binding)

 end

end;

##################################




$skala=1;

Konwencja=:rosyjska

class Array

  def to_russian
     case Konwencja
        when :rosyjska  : [$skala*(self[1]-self[0]),$skala*(self[1]+self[0]) ] 
        when :francuska : [$skala*(self[1]),$skala*(self[0]) ]
     end
  end

  def to_coordinate_string
    x,y=*self.to_russian
    "(#{x},#{y})"  
  end


  def linia(opcje="")
        "\\draw[#{opcje}]" << collect { |p| p.to_coordinate_string}.join(" -- ") << ";\n"   
  end


  def wielokat(opcje="")
        "\\fill[#{opcje}]" << collect { |p| p.to_coordinate_string}.join(" -- ") << " -- cycle;\n"   
  end


end



#######################################################3



require 'posortowane-tablice'
require 'yaml'

Infinity=10000000000000000


def rsk(permutacja)
    insertion=[]
    recording=[]
    jeu=[]
    historia=[]   # kolejność, w jakiej dodawane są klatki

    permutacja.each_with_index do |entry,index|
         k=0          
         bumper=entry
         loop do
           if insertion[k].nil? then insertion[k]=[]; recording[k]=[]; jeu[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)

           insertion[k][gdzie],bumper = bumper, insertion[k][gdzie] 

           if bumper.nil? then 
                            recording[k][gdzie]=index 
                            jeu[k][gdzie]=entry 
                            historia << [k,gdzie]
                            break 
                          end

           k=k+1
         end
   end
   [insertion,recording,jeu,historia]
end


def element(tableau,wiersz,kolumna)
    (tableau[wiersz] or [])[kolumna]
end



def jeu_de_taquin(tableau, type=:acyclic)
   tableau=tableau.collect{|l| l.dup}
   minimum=element(tableau,0,0)
   trajektoria=[]
   i=0
   j=0

   loop do
        trajektoria << [i,j]
        break if element(tableau,i,j+1).nil? and element(tableau,i+1,j).nil?
        #  działa dla SemiStandard tableax.
        if (element(tableau,i,j+1) or Infinity) < (element(tableau,i+1,j) or Infinity) 
                           then i1,j1=[i,j+1] else i1,j1=[i+1,j] end
        tableau[i][j]=element(tableau,i1,j1)
        i,j=[i1,j1]
   end

   if type==:cyclic then tableau[i][j]=minimum+IloscKlatek
                    else tableau[i].pop end

   [tableau,trajektoria] 
end




=begin
Seed = srand % 1000
Seed = 884   ### dobra wartość
srand(Seed)
=end
Seed=""


=begin
IloscKlatek=300
permutacja=(1..IloscKlatek).collect { rand }
permutacja[0]=0.5
i,r,j,h=rsk(permutacja)
=end

require 'yaml'
permutacja,tableau=YAML::load(File.open('permutation_tableau_20000.txt','r').read)
permutacja=permutacja[0..300]
i,r,j,h=rsk(permutacja)
jeu=jeu_de_taquin(r).last


size=15
rysunek = "\\clip (-#{size},-0.5) -- (#{size},0.5) -- (#{size},#{size}) -- (-#{size},#{size}); \n"
rysunek << (0..size).collect {|n| [ [0,n],  [size,n] ].linia("ultra thin") }.join
rysunek << (0..size).collect {|n| [ [n,0],  [n,size] ].linia("ultra thin") }.join
rysunek << (0..size).step(5).collect {|n| [ [0,n],  [size,n] ].linia("thick") }.join
rysunek << (0..size).step(5).collect {|n| [ [n,0],  [n,size] ].linia("thick") }.join
rysunek << h.each_with_index.collect { |box, n|  x,y=box;  '\draw '+[x+0.5, y+0.5].to_coordinate_string+" node {#{(n+1)}};\n"}.join
#rysunek << jeu.collect{|x,y| [x+0.5,y+0.5]}.linia("blue,opacity=0.3,line width=5pt")
rysunek << jeu.collect{|x,y| [ [x,y], [x+1,y], [x+1,y+1], [x,y+1] ].wielokat("blue,opacity=0.3") }.join


skalka=5.0
puts rysunek.latex_file( :tikzpicture=>"scale=#{skalka/size}"  )


