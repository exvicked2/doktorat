#!/usr/bin/env ruby


require 'erb'


####################################

class String

def latex_file(opcje) 



template = %q{
\documentclass[12pt]{amsart}
\usepackage{amsmath}

\usepackage{eucal}
\usepackage{times}
\usepackage{euler}
\usepackage{eufrak}

\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\usepackage{tikz}

\begin{document}

\tiny
\begin{figure}
\centering
\begin{tikzpicture}[<%= opcje[:tikzpicture] %>]
<%=   self    %> \end{tikzpicture}
\caption{Bumping routes}
\end{figure}
\end{document}

}

    rtex = ERB.new(template, 0, "%<>");

    rtex.result(binding)

 end

end;

##################################




$skala=1;

Konwencja=:francuska

class Float

    def to_s
        "%6f" % self
    end
end



class Array

#   def to_russian(konwencja=:rosyjska)
#      case konwencja
#         when :rosyjska  : [$skala*(self[1]-self[0]),$skala*(self[1]+self[0]) ] 
#         when :francuska : [$skala*(-self[0]),$skala*(self[1]) ]
#      end
#   end

  def to_coordinate_string(konwencja=:rosyjska)
    x,y=*self
    "(#{x*Skala},#{y*Skala})"  
  end


  def linia(opcje="")
        "\\draw[#{opcje}]" << collect { |p| p.to_coordinate_string}.join(" -- ") << ";\n"   
  end


  def wielokat(opcje="")
        "\\fill[#{opcje}]" << collect { |p| p.to_coordinate_string}.join(" -- ") << " -- cycle;\n"   
  end


end



#######################################################3



require 'posortowane-tablice'
require 'yaml'
# require 'grafika'


Infinity=10000000000000000


def rsk(permutacja)
    insertion=[]
    recording=[]
    jeu=[]
    historia=[]   # kolejność, w jakiej dodawane są klatki

    permutacja.each_with_index do |entry,index|
         k=0          
         bumper=entry
         loop do
           if insertion[k].nil? then insertion[k]=[]; recording[k]=[]; jeu[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)

           insertion[k][gdzie],bumper = bumper, insertion[k][gdzie] 

           if bumper.nil? then 
                            recording[k][gdzie]=index 
                            jeu[k][gdzie]=entry 
                            historia << [k,gdzie]
                            break 
                          end

           k=k+1
         end
   end
   [insertion,recording,jeu,historia]
end



def bumping(insertion,entry)
         k=0          
         bumper=entry
	 historia=[];
	 
         loop do
           if insertion[k].nil? then insertion[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)
	   historia<< [k,gdzie]

           bumper =insertion[k][gdzie] 

           break if bumper.nil? 

           k=k+1
         end
 
	 historia
end  






# numberofboxes=1000000
# permutacja=(1..numberofboxes).collect { rand }
# i,r,j,h=rsk(permutacja)
# 
# puts YAML::dump(i)
# 
# 
# exit

# rozmiar=460
# numberofboxes=50000
# insertion=YAML::load(File.open('insertion-tableau50000.txt','r').read)

Skala=0.1
skala=0.5
step=10
rozmiar=200
numberofboxes=10000
insertion=YAML::load(File.open('insertion-tableau10000.txt','r').read)

rysunek=""






rysunek << "\\draw (0,#{rozmiar*Skala}) -- (0,0) -- (#{rozmiar*Skala},0);"

0.step(rozmiar,step) {|x| rysunek << "\\draw (#{x*Skala},20pt) -- (#{x*Skala},-60pt) node[anchor=north] {#{x}}; \n" }
0.step(rozmiar,step) {|x| rysunek << "\\draw (20pt,#{x*Skala}) -- (-60pt,#{x*Skala}) node[anchor=east] {#{x}}; \n" }


wierzcholki=[[0,0]];
(insertion + [[]]).collect{|row| row.length}.each_with_index{|e,i| wierzcholki << [e,i];  wierzcholki << [e,i+1]}; 
rysunek <<  "\\draw " << wierzcholki.collect { |p| p.to_coordinate_string}.join(" -- ") << ";\n"   



numbers=(1..99).to_a.map{|n| (n/100.0) }

# colors=(%w{Black Blue Red Green Rhodamine Maroon Plum Bittersweet Cerulean Black Blue Red Green Rhodamine Maroon Plum Bittersweet Cerulean Black Blue Red Green Rhodamine Maroon Plum Bittersweet Cerulean}*10)[0...numbers.length]

colors=(%w{Blue Red Green}*100)[0...numbers.length]


# p wierzcholki.uniq
                                                   
                                                   
    
  
[numbers,colors].transpose.each { |entry,color|

rysunek << "\\begin{scope}[fill=#{color}] \n"
rysunek << bumping(insertion,entry).map{|i,j| "\\fill[opacity=0.2] #{[j,i].to_coordinate_string} rectangle #{[j+1,i+1].to_coordinate_string}; \n" }.join
rysunek << "\\end{scope} \n" 
rysunek << "\\draw[#{color},ultra thin] plot[smooth,scale=#{Skala*Math.sqrt(numberofboxes.to_f*entry)   }] file {bumpingcurve.txt}; \n"                                                                                         }


                                                                                         

skalka=5.0
puts rysunek.latex_file( :tikzpicture=>"scale=#{skala}"  )
