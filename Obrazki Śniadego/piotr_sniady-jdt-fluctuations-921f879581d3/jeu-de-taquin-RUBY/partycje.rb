##### partycje, podzbiory, wolne kumulanty
##### algorytmy chyba poprawne, ale powolne!

class Array
  def podzbiory
    if self.length==0 then return [[]] end;
    
    krotkie=self.dup;
    co=krotkie.pop;
    
    podzbiory=krotkie.podzbiory;
    
    podzbiory + podzbiory.map { |podzbior| podzbior+[co] } 
    
  end
end;


def rozbicia_na_nieujemne(co,liczba_skladnikow)
  return [ [co] ] if liczba_skladnikow==1
  
  lista=Array.new
  for ostatni in 0..co do 
    rozbicia_na_nieujemne(co-ostatni,liczba_skladnikow-1).each {|el| lista << ( el << ostatni )}
  end
  
  lista
end



def rozbicia(co,minimum,liczba_skladnikow=nil)
  if liczba_skladnikow.nil? then
             wynik=Array.new
             for liczba in 1..co / minimum do
                wynik = wynik + rozbicia(co,minimum,liczba)
             end
             return wynik
  end
   
  return [ [co] ] if (liczba_skladnikow==1) and (co>=minimum)
  return [      ] if (liczba_skladnikow==1) 
  
  lista=Array.new
  for ostatni in minimum..co do 
    rozbicia(co-ostatni,minimum,liczba_skladnikow-1).each {|el| lista << ( el << ostatni )}
  end
  
  lista
end





class Array
### 2partycje 2n elementow
  def nc_parowanie 
    
    if self==[] then return [ [] ] end;

    n=self.length;

    (0..n / 2 -1).collect { |j| i=2 * j; 
       p1=self[1..i].nc_parowanie; p2=self[i+2..n-1].nc_parowanie;
       p1.collect { |par1| p2.collect{ |par2|  
            [ [self[0],self[i+1]] ] + par1 + par2 } }.zlacz 
	}.zlacz
  end


# partycje zbioru 0..2n-1, bloki posortowane wg lewych nog
  def dwupartycja_w_bloki


#     indeksy=Array.new(self.length,0);
    
    iloscblokow=0;
    dlugoscblokow=[];
    indeksy=Array.new;
    
    self.each { |a,b|  
      if a.modulo(2)==0 then indeksy[a / 2]= iloscblokow;
                             dlugoscblokow[iloscblokow] = 1; 
                             iloscblokow+=1;
                        else indeksy[b / 2]= indeksy[(a-1) / 2]; 
			     dlugoscblokow[indeksy[b / 2]]+=1; 
			end; }
				
    dlugoscblokow;
  end



### kumulanty i momenty liczymy od zerowej.
#### powolne.
#   def wolne_kumulanty
#   
#     kumulanty=Array.new(self.length,0);
#   
#     for i in 1...self.length do
#       kumulanty[i]=self[i]-
#         (0...2*i).to_a.nc_parowanie.collect { |part|  part.dwupartycja_w_bloki.map {
#             |dbloku| kumulanty[dbloku] }.iloczyn }.suma   
#     end 
#   
#     kumulanty;  
#   
#   end


  def wolne_kumulanty

    kum=Array.new(length,0)
    mom=self
    kum[0]=0
  
    for numer in 1...length do 
          kum[numer]=mom[numer]-(1..numer-1).sumuj {|dlugosc_bloku| 
                    kum[dlugosc_bloku] * rozbicia_na_nieujemne(numer-dlugosc_bloku,dlugosc_bloku).sumuj{
                             |rozbicie| rozbicie.mnoz{|indeks| mom[indeks]} }}
    end
    kum
  end


# dane: wolne kumulanty
  def liczby_Gouldena_Rattana
     gr=Array.new(length,0)
     gr[0]=1
     for numer in 2...length do
            gr[numer]=rozbicia(numer,2).sumuj{|indeksy| indeksy.mnoz{|indeks| (indeks-1)*self[indeks]}}
     end  
     gr
  end


# na podstawie zadanych wolnych kumulant liczymy momenty
  def momenty_wolne

    kum=self
    mom=Array.new(length,0)
    mom[0]=1
  
    for numer in 1...length do 
          mom[numer]=(1..numer).sumuj {|dlugosc_bloku| 
                    kum[dlugosc_bloku] * rozbicia_na_nieujemne(numer-dlugosc_bloku,dlugosc_bloku).sumuj{
                             |rozbicie| rozbicie.mnoz{|indeks| mom[indeks]} }}
    end
    mom
  end
end

