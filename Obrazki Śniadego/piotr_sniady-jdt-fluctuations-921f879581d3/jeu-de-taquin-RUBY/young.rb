#!/usr/bin/env ruby


# uniwersalny plik zawierajacy wszystkie odniesienia.

require './grafika'
require 'yaml'

###################################################
###
### instrukcja obslugi.

# class Array; 	
# 		iloczyn;  			
# 		suma;  
# 		zlacz;				Join zadanej tablicy tablic
# 
# 
# 
# 
# class String
# 		eps_file			tworzy plik eps
# 		latex_file			tworzy plik LaTeX
# 
# 
# class GrafikaEps < String
# 
# 						kontener do grafiki eps
#   		initialize(x1,y1,x2,y2)		inicjuje
# 		latex_picture			generuje String zawierajacy PSPICTURE
#  
# class Array
# 
# 						miare dyskretna traktuje jako liste par [pdbo,co]
# 		losuj				losuje 
# 		momenty				momenty zadanej miary dyskretnej


###############################

require './partycje'

# class Array
# 		podzbiory
#  		nc_parowanie 
# 		dwupartycja_w_bloki		kazda NC dwupartycja zadaje NC partycje. Tu liczymy dlugosci jej blokow
# 
# 		wolne_kumulanty			wolne kumulanty zadanego ciagu momentow.
# 
# 						UWAGA: byc moze uzywam nietypowej notacji dla momentow! 
# 						momenty[0]=1, momenty[1]=pierwszy moment, etc.


##############################

require './posortowane-tablice'


# class Array
# 
# 		indeks_rsk(co)			indeks pierwszego elementu WIEKSZEGO od co
#  		posortowane_zawiera?(co)
# 		wstaw_posortowane (co)
# 		usun(co)


################################


# 		losowa_permutacja(n)
# 		permutacjaKnutha		przerabia permutacje w permutacje Knutha.
# 		rsk(permutacja)
#  		rsk_jeden_wiersz(permutacja)
# 
# 		losowy_diagram_plancherela(n)
# 
# 
# class Array
# 
# 		wstaw_contents (contents,co)	do tableaux wstawiamy "co" do klatki z ustalonym "contents"
# 
# 
# 
# 
# class DiagramYounga < Array
# 
# 		ilosc_klatek	
# 		ilosc_kolumn
# 		ilosc_wierszy
# 		momenty(ile)			momenty miary przejscia
# 		wolne_kumulanty(ile)
# 		przekatna(content)
# 		grubosc
# 		transponuj
# 		bounding_box_diagramu		wynikiem jest GrafikaEps
# 		w_Kerova
# 		rysunek(opcje="")
# 		profil(opcje="")
# 
# class TableauYounga < Array
# 		ilosc_klatek
# 		w_Younga
# 		w_Kerova
# 		grubosc
# 		ilosc_kolumn
# 		ilosc_wierszy
# 		momenty(ile)
# 		wolne_kumulanty(ile)
# 		usun_duze_klatki(granica)
# 		transponuj
# 		bounding_box
# 		rysunek
# 		kolorowane_tableau
# 		rysunek_beamer			stara procedura, niekompatybilna z reszta
# 
# 
# class DiagramKerova < Array
# 
# 		w_Younga
# 		ilosc_klatek
# 		transponuj
# 		usun_klatke(content)
# 		miara_koprzejscia
# 		miara_przejscia
# 		momenty(ile)
# 		wolne_kumulanty(ile)
# 		losuj_najwieksza_klatke
# 		losuj_contents




### uwaga: uzywamy nietypowych wspolrzednych (wiersz,kolumna)
### wszelkie skalowania musza isc przez to_russian

# 		to_russian
# # 		to_coordinate_string
# 		linia(p1,p2,opcje="")
# 		klatka_tableau (x,y,zawartosc)



# require 'testy_statystyczne.rb'

##### nie ma opisu pliku TESTY.RB

# typowy_diagram


def partycje_liczby_ograniczone(n,maks)
  return [[]] if n==0
  lista=Array.new
  for pierwszy in 1..[maks,n].min
     partycje_liczby_ograniczone(n-pierwszy,pierwszy).each{|partycja| lista << (partycja << pierwszy) }
  end
  lista
end


def partycje_liczby(n)
  partycje_liczby_ograniczone(n,n).map{|p| p.reverse}
end



