#!/usr/bin/env ruby

require 'erb'


class Array
  def cumulative_sum
    sum = 0
    self.map{|x| sum += x}
  end
end


class Float

    def to_s
        "%2.6f" % self
    end
end

require './posortowane-tablice'
require 'yaml'
require './young'


Infinity=10000000000000000


def rsk(permutacja)
    insertion=[]
    recording=[]
    jeu=[]
    historia=[]   # kolejność, w jakiej dodawane są klatki

    permutacja.each_with_index do |entry,index|
         k=0          
         bumper=entry
         loop do
           if insertion[k].nil? then insertion[k]=[]; recording[k]=[]; jeu[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)

           insertion[k][gdzie],bumper = bumper, insertion[k][gdzie] 

           if bumper.nil? then 
                            recording[k][gdzie]=index 
                            jeu[k][gdzie]=entry 
                            historia << [k,gdzie]
                            break 
                          end

           k=k+1
         end
   end
   [insertion,recording,jeu,historia]
end







IloscKlatek=100000

cdfs=[]

100.times do
        permutacja=(1..IloscKlatek).collect { rand }
        i,r,j,h=rsk(permutacja)

        transition= TableauYounga.new(i).w_Kerova.miara_przejscia
        y=([0]+transition.transpose.first.cumulative_sum)[0..-2]
        x=transition.transpose.last

        lista=[x,y].transpose

        my_x=0

        cdf= lista.find{|x,y| x>= my_x}.last
        cdfs << cdf
end

p cdfs

