#!/usr/bin/env ruby

require 'erb'


class Array
  def cumulative_sum
    sum = 0
    self.map{|x| sum += x}
  end
end


class Float

    def to_s
        "%2.6f" % self
    end
end

require './posortowane-tablice'
require 'yaml'
require './young'


Infinity=10000000000000000


def rsk(permutacja)
    insertion=[]
    recording=[]
    jeu=[]
    historia=[]   # kolejność, w jakiej dodawane są klatki

    permutacja.each_with_index do |entry,index|
         k=0          
         bumper=entry
         loop do
           if insertion[k].nil? then insertion[k]=[]; recording[k]=[]; jeu[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)

           insertion[k][gdzie],bumper = bumper, insertion[k][gdzie] 

           if bumper.nil? then 
                            recording[k][gdzie]=index 
                            jeu[k][gdzie]=entry 
                            historia << [k,gdzie]
                            break 
                          end

           k=k+1
         end
   end
   [insertion,recording,jeu,historia]
end



def newbox(insertion,entry)
         k=0;
	 gdzie=0;
         bumper=entry
	 
         loop do
           if insertion[k].nil? then insertion[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)

	   bumper = insertion[k][gdzie] 

           break if bumper.nil? 

           k=k+1
         end
#    [gdzie,k]
       gdzie-k
end  






IloscKlatek=10000
skala=1.0
permutacja=(1..IloscKlatek).collect { rand }
i,r,j,h=rsk(permutacja)

infinity=(2.6*Math.sqrt(IloscKlatek));

    
# IloscKlatek=250000
# Infinity=(2.4*Math.sqrt(IloscKlatek)).round;
# i=YAML::load(File.open('insertion-tableau250000.txt','r').read)




# i.each{|line| puts line.join("    ") }



lista =([0]+i.first).collect{|w|  [newbox(i,w),w] }
# p lista
h=Hash[*lista.reverse.flatten]
# p h
lista=h.to_a.sort_by{|a,b| a}
# p lista



final_points= lista + [[infinity,1]]
# p final_points

initial_points = [ [-infinity]+lista.transpose.first, lista.transpose.last+[1] ].transpose
# p initial_points


#puts "\\draw[red,opacity=0.5,very thick]\n"
#puts [initial_points, final_points].transpose.collect{|p1,p2| 
#"(#{p1.first/skala},#{p1.last}) -- (#{p2.first/skala},#{p2.last})"}.join(" -- \n")
#puts ";"


transition= TableauYounga.new(i).w_Kerova.miara_przejscia

#p transition
#puts "\n"
    
puts "\\draw[blue,opacity=0.5,very thick]\n"
    
points = [] 
    
x1=-infinity
x2=transition[0].last
x1=x1/Math.sqrt(IloscKlatek)
x2=x2/Math.sqrt(IloscKlatek)

#x1=x1*x1
#x2=x2*x2
    
points << "(#{x1},0) --  (#{x2},0) "

big_sum=0
    

for index in 0...transition.length do
    sum=0
    
    for i1 in 0..index do
        for i2 in index+1...transition.length do
            sum+=  transition[i1].first * transition[i2].first / (1+transition[i2].last-transition[i1].last)
        end
    end
    
    x1=transition[index].last
    if index==transition.length-1 then x2=infinity else x2=transition[index+1].last end
        
    big_sum+= (x2-x1)*sum    
        
    x1=x1/Math.sqrt(IloscKlatek)
    x2=x2/Math.sqrt(IloscKlatek)
        
#    x1=x1*x1
#    x2=x2*x2

        
    sum=sum * Math.sqrt(IloscKlatek)

#    sum=Math.sqrt(sum)
        
    points << " (#{x1},#{sum}) --  (#{x2},#{sum}) "
end
    
#puts big_sum

puts points.join("--")
puts (";\n")
        
        
        
        
    
#y=([0]+transition.transpose.first.cumulative_sum)[0..-2]
#x=transition.transpose.last
#
#lista=[x,y].transpose
#
#final_points= lista + [[infinity,1]]

    
    # p final_points

#initial_points = [ [-infinity]+lista.transpose.first, lista.transpose.last+[1] ].transpose
## p initial_points
#
#puts "\\draw[blue,opacity=0.5,very thick]\n"
#puts [initial_points, final_points].transpose.collect{|p1,p2| 
#"(#{p1.first/skala},#{p1.last}) -- (#{p2.first/skala},#{p2.last})"}.join(" -- \n")
#puts ";"
