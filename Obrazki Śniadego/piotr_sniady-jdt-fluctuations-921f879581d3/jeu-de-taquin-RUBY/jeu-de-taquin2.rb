#!/usr/bin/env ruby

require 'posortowane-tablice'
require 'yaml'

Infinity=10000000000000000



def rsk(permutacja)
    insertion=[]
    recording=[]
    jeu=[]
    historia=[]   # kolejność, w jakiej dodawane są klatki

    permutacja.each_with_index do |entry,index|
         k=0          
         bumper=entry
         loop do
           if insertion[k].nil? then insertion[k]=[]; recording[k]=[]; jeu[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)

           insertion[k][gdzie],bumper = bumper, insertion[k][gdzie] 

           if bumper.nil? then 
                            recording[k][gdzie]=index 
                            jeu[k][gdzie]=entry 
                            historia << [k,gdzie]
                            break 
                          end

           k=k+1
         end
   end
   [insertion,recording,jeu,historia]
end



# list of coordinates of bumped elements and the bumped elements themselves.
# including the place where the box was added.
def rsk_one_insertion(insertion,entry)

    bumps=[]   # bumped elements
    historia=[]
    k=0          
    bumper=entry

    until bumper.nil? do
           gdzie=(insertion[k] or []).indeks_rsk(bumper)
           bumper =(insertion[k] or [])[gdzie]

           bumps << bumper unless bumper.nil?
           historia << gdzie 
           k=k+1
    end

    [historia,bumps]

end





def element(tableau,wiersz,kolumna)
    (tableau[wiersz] or [])[kolumna]
end




def jeu_de_taquin(tableau, type=:acyclic)
   tableau=tableau.collect{|l| l.dup}
   minimum=element(tableau,0,0)
   trajektoria=[]
   i=0
   j=0

   loop do
        trajektoria << [i,j]
        break if element(tableau,i,j+1).nil? and element(tableau,i+1,j).nil?
        #  działa dla SemiStandard tableax.
        if (element(tableau,i,j+1) or Infinity) < (element(tableau,i+1,j) or Infinity) 
                           then i1,j1=[i,j+1] else i1,j1=[i+1,j] end
        tableau[i][j]=element(tableau,i1,j1)
        i,j=[i1,j1]
   end

   if type==:cyclic then tableau[i][j]=minimum+IloscKlatek
                    else tableau[i].pop end

   [tableau,trajektoria] 
end



##############################################################################################


class Float
    def to_s
        "%1.2f" % self
    end
end

class Array

    def content
        x,y=self
        y-x
    end

end


########################################################################################

def porownaj_tableau(r1,r2,delta=0)
    (0...[r1.length,r2.length,40].min).each do |k|
        puts (0...[r1[k].length,r2[k].length].min).collect { |l|
                n=r1[k][l]-r2[k][l]+delta;  n==0 ? " "*4 : "%4i" % n   }.join[0...120]
    end
end



#############################################################################################
# dodaję na końcu posortowany ciąg. Czy klatki są dodane od lewej do prawej? (Tak)

=begin
IloscKlatek1=5
IloscKlatek2=100
Colors=0
permutacja=(1..IloscKlatek1).collect { rand(Colors) } + (1..IloscKlatek2).collect { rand(Colors) }.sort
i,r,j,h=rsk(permutacja)
cont=h[-IloscKlatek2..-1].map {|box| box.content }
p cont==cont.sort
=end

#############################
## dodaję nowy element do permutacji, ale w środku.
# zmiany w tableaux są dramatyczne
=begin
IloscKlatek=1000
Colors=0
insert=IloscKlatek/10
permutacja1=(1..IloscKlatek).collect { rand(Colors) }
permutacja2=permutacja1.dup.insert(insert,0.5)
i,r1,j,h=rsk(permutacja1)
i,r2,j,h=rsk(permutacja2)

(0...[r1.length,r2.length,40].min).each do |k|
    puts (0...[r1[k].length,r2[k].length].min).collect { |l|
            n=r1[k][l]-r2[k][l] + (r1[k][l]>=insert ? 1 : 0);  [0].member?(n) ? " "*2+"-" : "%3i" % n   }.join[0...150]

end
=end



#######################################################################
## dodaję dwa elementy na POCZĄTKU permutacji. Na ile wynik RSK zależy od ich kolejności?

IloscKlatek=100
Colors=0
permutacja=(1..IloscKlatek).collect { rand(Colors) }

a,b=0.4,0.5

i,r0,j,h=rsk(permutacja)
i,r1,j,h=rsk([a]+permutacja)
i,r2,j,h=rsk([b]+permutacja)
i,r3,j,h=rsk([a,b]+permutacja)
i,r4,j,h=rsk([b,a]+permutacja)


#porownaj_tableau(r0,r1,1)
#porownaj_tableau(r0,r2,1)
porownaj_tableau(r1,r2,0)
puts "************************************"
#porownaj_tableau(r0,r3,2)
#porownaj_tableau(r0,r4,2)
porownaj_tableau(r3,r4)



##############################################################
#    dodaję jeden element do insertion tableau; które elementy zostały przesunięte???
=begin
loop do
    permutacja=(1..IloscKlatek).collect { rand(Colors) }
    i,r,j,h=rsk(permutacja)
    h,b=rsk_one_insertion(i,0.5)
    p h
    p b
    p "\n"
end    
=end






########################################################
=begin
#    Dwa kolory, różne prawdopodobieństwa. Dla proba >= 0.5 metoda Śniadego pozwala na zrekonstruowanie permutacji z trajektorii JDT
proba=0.7
permutacja=(1..IloscKlatek).collect { if rand<proba then 0 else 1 end }
=end

=begin
#   Czy rekonstrukcja z JDT pokrywa się z rzeczywistością?
readout=(1..IloscKlatek).collect { r,t = jeu_de_taquin(r,:acyclic) 
             t.last.first  }

p [readout, permutacja].transpose.collect{|a,b| a-b }
=end 
#########################################################






###################################################################################
# szukam par klatek, które wylądowały w przeciwnej kolejności, niż byśmy się spodziewali 
=begin
delta=2
loop do
permutacja=(1..IloscKlatek).collect { rand(Colors) }
i,r,j,historia=rsk(permutacja)

bad=(0...IloscKlatek-delta).select { |index|
        (historia[index].content < historia[index+delta].content) ^ (permutacja[index] < permutacja[index+delta]) }   

    
# next if bad.empty?
#p permutacja
p bad.length
bad.collect {|index|  [index, [permutacja[index], permutacja[index+delta]], [historia[index].content, historia[index+delta].content] ] }.transpose.each{|line| p line }

#p i,r,j
end    
=end
######################################################################################





################################################################
## popatrzmy gdzie wylądowało kilka ostatnich klatek. Czy ich kolejność mniej-więcej zgadza się z naszą intuicją?
=begin
permutacja=(1..IloscKlatek).collect { rand(Colors) }
i,r,j,historia=rsk(permutacja)

indeksy=IloscKlatek-Math::sqrt(IloscKlatek)/4.round ... IloscKlatek
p [ historia[indeksy].collect{|box| box.content }, permutacja[indeksy] ].transpose.sort_by {|a,b| b}.transpose.first
=end






=begin
j.reverse.each {|l| puts l.join(" ") }
r.reverse.each {|l| puts l.collect{|n| "%4i" % n}.join(" ") }
=end





###########################################
# test (fałszywej) hipotezy o stożkach Einsteina
=begin
loop {
permutacja=(1..IloscKlatek).collect { rand(Colors) }


i,r,j=rsk(permutacja)
next if element(j,1,0).nil?
next if element(j,0,1).nil?
ref     =j[1].first
einstein=j[0][1..-1]
next unless ref > einstein.min

p permutacja
p r,j
p "*************"
}
=end
################################################







####################################################
=begin
# l zawiera interesujące nas tableau. Wycinamy z niego największy możliwy kwadrat
# typowe zastosowanie:   j = diagram "jeu"

#  wyznaczamy największy kwadrat, który mieści się w tableux
best_box= ( [[]] + j).collect{|l| l.length}.each_with_index.map{|l| l.min}.max

dane=j[0...best_box].collect{|l| l[0,best_box-1]}

dane.each{|l| puts l.join(" ") }
=end
##################################################






###############################################
# rysowanie trajektorii
=begin
30.times{
t,trajektoria = jeu_de_taquin(t) 
trajektoria.each{ |x,y| puts "%5d %5d" % [x, y] }
}
=end



=begin
10000.times{
t,trajektoria = jeu_de_taquin(t) 
x,y=trajektoria.last
a,b=[x+y,x-y]
puts b.to_f / a
}
=end

=begin
10000.times{
t,trajektoria = jeu_de_taquin(t) 
x,y=trajektoria.last
a,b=[x+y,x-y]
puts b
}
=end
