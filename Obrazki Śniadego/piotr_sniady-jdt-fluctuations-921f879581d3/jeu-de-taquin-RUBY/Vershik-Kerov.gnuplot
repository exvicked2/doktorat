set table "vershik-kerov.txt"
set format "% .5f"
set samples 100
set parametric

set dummy z


### Vershik Kerov curve
VK(s)=(2/pi)*( s*asin(s/2) + sqrt(4-s**2) )

skala=sqrt(1000)


zn(z)=z
tn(z)=VK(z)


xx(z)=(zn(z)+tn(z))/2
yy(z)=(-zn(z)+tn(z))/2


set trange [-2:2]
plot skala*xx(z),skala*yy(z)






