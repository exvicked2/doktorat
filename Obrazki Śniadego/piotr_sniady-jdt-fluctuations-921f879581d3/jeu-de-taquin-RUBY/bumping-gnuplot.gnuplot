set table "bumpingcurve.txt"
set format "% .5f"
set samples 100
set parametric

set dummy z

## Cumulative Distribution Function of Wigner semicircular
CDF(s) = 0.5 + s *sqrt(R**2-s**2)/(pi*R**2) + asin(s/R)/pi
R=2

### Vershik Kerov curve
VK(s)=(2/pi)*( s*asin(s/2) + sqrt(4-s**2) )

### we parametrize by the "normalized z coordinate"



level(z)= 1/CDF(z)
t(z)=VK(z)

zn(z)=z*sqrt(level(z))
tn(z)=t(z)*sqrt(level(z))

xx(z)=(zn(z)+tn(z))/2
yy(z)=(-zn(z)+tn(z))/2

#set trange [-2:2]
#plot z,CDF(z)

set trange [-1.9:2]
plot xx(z),yy(z)