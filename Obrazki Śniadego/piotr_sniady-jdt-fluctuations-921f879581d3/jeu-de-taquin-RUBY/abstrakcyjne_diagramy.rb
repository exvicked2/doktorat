#  abstrakcyjne procedury dotyczace diagramow

require './partycje'
require './posortowane-tablice'






# Miary atomowe
class Array

### parametrem jest tablica par [pdbo,co] 
  def losuj
 	 los=rand

	  self.each do |pdbo, wartosc| 
                      los=los-pdbo
                      if los<=0 then return(wartosc) end
                    end 
  end


### parametrem jest tablica par [pdbo,co] 
  def momenty(ile)
    (0..ile).collect { |n|  self.collect { |pdbo,co| pdbo* (co**n) }.suma }
  end
  
end


########################################################################################


def losowa_permutacja(n)
  (0..n).collect {|n| [rand,n] }.sort_by {|para| para[0]}.collect {|para| para[1]}
end

class Array
	def permutacjaKnutha
	  (0...self.length).collect {|n|  [n,self[n]]}
	end
end

class Array; def iloczyn; inject( 1 ) { |prod,x| prod*x }; end; 
             def suma;    inject( 0 ) { |suma,x| suma+x }; end; 
	     def zlacz;   inject([])  { |zlacze,x| zlacze+x }; end;
	     end

module Enumerable; def sumuj;     collect{|element| yield element}.suma end
                   def mnoz;  collect{|element| yield element}.iloczyn end 
                   end
                  
################################
# RSK

### ta procedura sobie nie poradzi, jesli moga wystepowac podwojne 
#   elementy



#       k=0
# 	while (wiersz.length>k) and (wiersz[k]<co) do  k=k+1  end
# 	k


def rsk(permutacja)
 a=TableauYounga.new;
 b=TableauYounga.new;

permutacja.permutacjaKnutha.each do |n,m|
         k=0          
         loop do
           if a[k].nil? then a[k]=[]; b[k]=[]; end 
           gdzie=a[k].indeks_rsk(m)

           a[k][gdzie],m = m, a[k][gdzie] 

           if m.nil? then b[k][gdzie]=n; break end
           k=k+1
         end
   end
 [a,b]
end


def rsk_jeden_wiersz(permutacja)
  wiersz=Array.new;

  permutacja.each do |m|
     gdzie=wiersz.indeks_rsk(m)
     wiersz[gdzie] = m 
   end

  wiersz.length
end





# # # # # # # # # # # # # # # # # # # # # # # # # # # 

def losowy_diagram_plancherela(n)
  rsk(losowa_permutacja(n))[0].w_Younga;
end


class DiagramYounga < Array

  def ilosc_klatek
    self.suma
  end

  def ilosc_kolumn
    self[0]
  end

  def ilosc_wierszy
    self.length
  end 

  def momenty(ile)
    self.w_Kerova.momenty(ile)
  end
  
  def wolne_kumulanty(ile)
    self.w_Kerova.wolne_kumulanty(ile)
  end


# Zeby dobrze dzialalo, musi byc content >= 0!!!!!
  def przekatna(content)
    (0...self.length).to_a.select { |n| self[n]-n>content }.length+content
  end


  def grubosc
    ( (0...self.length).collect {|n| n+self[n]} ).max
  end

  def transponuj
    self.w_Kerova.transponuj.w_Younga;
  end 




  def w_Kerova
    min = (0...self.length).collect {|n| self[n]-n };
    min << -self.length 
    maks= (0...self.length).collect {|n| self[n]-n-1 };
   
    DiagramKerova.new([(min-maks).sort,(maks-min).sort])
  end


end




###############################
# tableau Younga
class TableauYounga < Array





# class Array
### do tableaux wstawiamy "co" do klatki z ustalonym "contents"
   def wstaw_contents (contents,co)

   wiersz=-1;   
   begin   
      wiersz+=1; 
      if (self[wiersz].nil?) then self[wiersz]=[];  end 
   end while ( (self[wiersz].length - wiersz) > contents)

   self[wiersz] << co
      
   end



  def ilosc_klatek
    self.w_Younga.suma
  end 


  def w_Younga
    DiagramYounga.new(self.collect { |wiersz| wiersz.length })
  end


  def w_Kerova
    self.w_Younga.w_Kerova
  end


  def grubosc
    self.w_Younga.grubosc
  end


  def ilosc_kolumn
    self.w_Younga.ilosc_kolumn
  end


  def ilosc_wierszy
    self.w_Younga.ilosc_wierszy
  end


  def momenty(ile)
    self.w_Kerova.momenty(ile)
  end

  
  def wolne_kumulanty(ile)
    self.w_Kerova.wolne_kumulanty(ile)
  end

  
  def usun_duze_klatki(granica)
    TableauYounga.new( 
       self.collect { |wiersz| wiersz.dup.delete_if {|element| element >= granica } }.delete_if { |wiersz| wiersz.length==0 })
       # albo?  delete([])
  end  


  def usun_duze_klatki_zostaw_zero(granica)
    TableauYounga.new( 
       self.collect { |wiersz| wiersz.dup.delete_if {|element| element >= granica } } )
  end  



  def transponuj
    pom=TableauYounga.new;
    for i in 0...self.length do
      for j in 0...self[i].length do
        if pom[j].nil? then pom[j]=Array.new; end;
        pom[j][i]=self[i][j];
      end
    end
    pom;
  end 


  def to_s
    self.collect { |wiersz| wiersz.join(" ") }.join("; ")	
  end


  def klatki
    k = Array.new;

    self.each_index {|n|
        self[n].each_index { |m|
           k << [n,m,self[n][m]] }} 

    k.sort_by { |x,y,zawartosc| zawartosc }
  end


end

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

class DiagramKerova < Array

   def w_Younga
     min,maks = *self;
     shift=0;

     DiagramYounga.new( 
       ((0...maks.length).collect { |n|  
          shift+=maks[n]-min[n];
          Array.new(min[n+1]-max[n]) {shift}  }
        ).flatten.reverse);
   end

   def ilosc_klatek
     min,maks = *self;
     shift=0;
     suma=0;
      
       maks.each_index { |n|  
          shift+=maks[n]-min[n];
          suma+=(min[n+1]-max[n]) * shift  }

     suma 
   end

   def transponuj
     min,maks = *self
     DiagramKerova.new([min.collect { |x| -x }.reverse, 
                        maks.collect { |x| -x }.reverse ]) 
   end 


   def usun_klatke(content)
     min,maks= *self.dup;  ##### USUNAC CZY NIE USUWAC DUP  ####


     if min.posortowane_zawiera?(content-1) 
           then min.usun(content-1) 
           else maks.wstaw_posortowane(content-1) end    

     if min.posortowane_zawiera?(content+1) 
           then min.usun(content+1) 
           else maks.wstaw_posortowane(content+1) end    

     maks.usun(content);
     min.wstaw_posortowane(content) 

     self[0]= min;
     self[1]= maks;

     self
   end




# # # # # # # # # # # # # # # # # #    MIARY


  def miara_koprzejscia
    minima,maksima = *self;
    ilosc_klatek=self.ilosc_klatek;

    ((0...maksima.length).map  { |n| 
       pom=maksima.dup; pom.delete_at(n);
       [ - minima.collect { |min| maksima[n]-min }.iloczyn.to_f / 
        (pom.collect{ |maks| maksima[n]-maks}.iloczyn * ilosc_klatek) , 
      maksima[n] ]  })
  end


  
  
  ##### DOSTOSOWANE DO DUŻYCH DIAGRAMÓW
  def miara_przejscia
    minima,maksima = *self;
#     ilosc_klatek=self.ilosc_klatek;

    ((0...minima.length).map  { |n| 
       pom=minima.dup; pom.delete_at(n);
       [ maksima.collect { |maks| (minima[n]-maks)/1000.0 }.iloczyn /    ##### DOSTOSOWANE DO DUŻYCH DIAGRAMÓW
        (pom.collect{ |min| (minima[n]-min)/1000.0}.iloczyn ) ,           ##### DOSTOSOWANE DO DUŻYCH DIAGRAMÓW
      minima[n] ]  })
  end


  def momenty(ile)
    self.miara_przejscia.momenty(ile);
  end



  def wolne_kumulanty(ile)
#     self.miara_przejscia.momenty(ile).wolne_kumulanty;
##### prawidlowe, ale wolne  
    momenty=self.miara_przejscia.momenty(ile);
#     [0,0,momenty[2],momenty[3],momenty[4]-2* momenty[2]**2]; 
    momenty.wolne_kumulanty
  end


  
  
  def losuj_najwieksza_klatke
    self.miara_koprzejscia.losuj 
  end



  def losuj_contents

    diagram=DiagramKerova.new([self[0].dup,self[1].dup]);
     
    contents=[];

    while diagram[1].length>0 
      klatka=diagram.losuj_najwieksza_klatke;
      contents << klatka;
      diagram.usun_klatke(klatka)
    end

    contents.reverse;  
  end


  def losowe_tableau
    contents=self.losuj_contents;
    
    tableau=TableauYounga.new;
    contents.each_index { |n|
       tableau.wstaw_contents(contents[n],n) }
    tableau;
  end


end

