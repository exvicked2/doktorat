#!/usr/bin/env ruby


require 'posortowane-tablice'
require 'yaml'

Infinity=10000000000000000


def rsk(permutacja)
    insertion=[]
    recording=[]
    jeu=[]
    historia=[]   # kolejność, w jakiej dodawane są klatki

    permutacja.each_with_index do |entry,index|
         k=0          
         bumper=entry
         loop do
           if insertion[k].nil? then insertion[k]=[]; recording[k]=[]; jeu[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)

           insertion[k][gdzie],bumper = bumper, insertion[k][gdzie] 

           if bumper.nil? then 
                            recording[k][gdzie]=index 
                            jeu[k][gdzie]=entry 
                            historia << [k,gdzie]
                            break 
                          end

           k=k+1
         end
   end
   [insertion,recording,jeu,historia]
end


def element(tableau,wiersz,kolumna)
    (tableau[wiersz] or [])[kolumna]
end



def jeu_de_taquin(tableau, type=:acyclic)
   tableau=tableau.collect{|l| l.dup}
   minimum=element(tableau,0,0)
   trajektoria=[]
   i=0
   j=0

   loop do
        trajektoria << [i,j]
        break if element(tableau,i,j+1).nil? and element(tableau,i+1,j).nil?
        #  działa dla SemiStandard tableax.
        if (element(tableau,i,j+1) or Infinity) < (element(tableau,i+1,j) or Infinity) 
                           then i1,j1=[i,j+1] else i1,j1=[i+1,j] end
        tableau[i][j]=element(tableau,i1,j1)
        i,j=[i1,j1]
   end

   if type==:cyclic then tableau[i][j]=minimum+IloscKlatek
                    else tableau[i].pop end

   [tableau,trajektoria] 
end


def random_semicircle
    begin
        x,y=(1..2).collect { 4* rand - 2 }
    end until x**2+y**2 < 4
    x
end






IloscKlatek=200000
permutacja=(1..IloscKlatek).collect { random_semicircle }
i,r,j,h=rsk(permutacja)
jeu=jeu_de_taquin(r).last

puts YAML::dump([permutacja,r])

