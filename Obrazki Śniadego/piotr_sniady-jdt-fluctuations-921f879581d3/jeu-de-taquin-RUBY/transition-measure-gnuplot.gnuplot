set format "% .5f"
set samples 100
set parametric

set dummy z

## Cumulative Distribution Function of Wigner semicircular
CDF(s) = 0.5 + s *sqrt(R**2-s**2)/(pi*R**2) + asin(s/R)/pi



set table "CDF-Wigner10000.txt"
R=2*sqrt(10000)
set trange [-R:R]
plot z,CDF(z)


set table "CDF-Wigner100.txt"
R=2*sqrt(100)
set trange [-R:R]
plot z,CDF(z)

