#!/usr/bin/env ruby

require 'erb'


class Array
  def cumulative_sum
    sum = 0
    self.map{|x| sum += x}
  end
end


class Float

    def to_s
        "%2.6f" % self
    end
end

require 'posortowane-tablice'
require 'yaml'
require 'young'


Infinity=10000000000000000


def rsk(permutacja)
    insertion=[]
    recording=[]
    jeu=[]
    historia=[]   # kolejność, w jakiej dodawane są klatki

    permutacja.each_with_index do |entry,index|
         k=0          
         bumper=entry
         loop do
           if insertion[k].nil? then insertion[k]=[]; recording[k]=[]; jeu[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)

           insertion[k][gdzie],bumper = bumper, insertion[k][gdzie] 

           if bumper.nil? then 
                            recording[k][gdzie]=index 
                            jeu[k][gdzie]=entry 
                            historia << [k,gdzie]
                            break 
                          end

           k=k+1
         end
   end
   [insertion,recording,jeu,historia]
end



def newbox(insertion,entry)
         k=0;
	 gdzie=0;
         bumper=entry
	 
         loop do
           if insertion[k].nil? then insertion[k]=[]; end 
           gdzie=insertion[k].indeks_rsk(bumper)

	   bumper = insertion[k][gdzie] 

           break if bumper.nil? 

           k=k+1
         end
#    [gdzie,k]
       gdzie-k
end  






IloscKlatek=10000
skala=1.0
permutacja=(1..IloscKlatek).collect { rand }
i,r,j,h=rsk(permutacja)

infinity=(2.6*Math.sqrt(IloscKlatek));

# IloscKlatek=250000
# Infinity=(2.4*Math.sqrt(IloscKlatek)).round;
# i=YAML::load(File.open('insertion-tableau250000.txt','r').read)




# i.each{|line| puts line.join("    ") }



lista =([0]+i.first).collect{|w|  [newbox(i,w),w] }
# p lista
h=Hash[*lista.reverse.flatten]
# p h
lista=h.to_a.sort_by{|a,b| a}
# p lista



final_points= lista + [[infinity,1]]
# p final_points

initial_points = [ [-infinity]+lista.transpose.first, lista.transpose.last+[1] ].transpose
# p initial_points


puts "\\draw[red,opacity=0.5,very thick]\n"
puts [initial_points, final_points].transpose.collect{|p1,p2| 
"(#{p1.first/skala},#{p1.last}) -- (#{p2.first/skala},#{p2.last})"}.join(" -- \n")
puts ";"


transition= TableauYounga.new(i).w_Kerova.miara_przejscia
y=([0]+transition.transpose.first.cumulative_sum)[0..-2]
x=transition.transpose.last

lista=[x,y].transpose

final_points= lista + [[infinity,1]]
# p final_points

initial_points = [ [-infinity]+lista.transpose.first, lista.transpose.last+[1] ].transpose
# p initial_points

puts "\\draw[blue,opacity=0.5,very thick]\n"
puts [initial_points, final_points].transpose.collect{|p1,p2| 
"(#{p1.first/skala},#{p1.last}) -- (#{p2.first/skala},#{p2.last})"}.join(" -- \n")
puts ";"
