#!/usr/bin/env ruby


#####################################

require 'erb'
require './abstrakcyjne_diagramy'


class Numeric

# Zaokraglanie
   def rn
     (self*1000.0).to_i / 1000.0
   end

end
   


$skala=1;

Konwencja=:rosyjska
Przedluzenie=0.9

class FrenchCoordinates < Array
  
  def to_screen
    x,y=*self
    case Konwencja
        when :rosyjska  then [$skala*(y-x),$skala*(x+y) ] 
        when :francuska then [$skala*x,$skala*y ]
    end
  end    

end  


class RussianCoordinates < Array
  
  def to_screen
    z,t=*self
    case Konwencja
        when :rosyjska  then [$skala*z,$skala*t ] 
        when :francuska then [$skala*(z+t)/2.0,$skala*(t-z)/2.0 ]
    end
    
  end  
  
end  





class Array

  def russian_to_french
     z,t=*self
     [(z+t)/2.0, (t-z)/2.0]
  end  

  def french_to_russian
     x,y=*self
     [x-y,x+y]
  end  
  
  
  def to_coordinate_string
    x,y=*self; 
#     .to_screen
    "(#{x},#{y})"  
  end

end




def linia(punkty,opcje="")
  "\\draw[#{opcje}]" << punkty.collect { |p| p.to_coordinate_string}.join(" -- ") << "; \n"   
end


def wielokat(wierzcholki,opcje="")
  "\\draw[#{opcje}]" << wierzcholki.collect { |p| p.to_coordinate_string}.join(" -- ") << " -- cycle ;\n" 
end


def clip(wierzcholki,opcje="")
  "\\clip[#{opcje}]" << wierzcholki.collect { |p| p.to_coordinate_string}.join(" -- ") << ";\n" 
end



def klatka_tableau (x,y,zawartosc)
  a = FrenchCoordinates.new([x+0.5,y+0.5]) 
  "\\draw #{a.to_coordinate_string} node {$#{zawartosc}$};\n"
end




class String


def latex_file(options="")
  template = %q{

\documentclass[12pt]{amsart}
\usepackage{amsmath}

\usepackage{tikz}
\usetikzlibrary{decorations.markings}

\begin{document}
\begin{tikzpicture}[<%= options   %>]
<%=   self    %> 
\end{tikzpicture}

\end{document}

}

    rtex = ERB.new(template, 0, "%<>");

    rtex.result(binding)

 end

end;






class DiagramYounga


# wielokat przedstawiajacy obrys diagramu Younga
  
  def wypukle_rogi
      wyp=(self).each_with_index.to_a.collect{|l,i| FrenchCoordinates.new([l,i+1])}
      wkl=self.each_with_index.to_a.collect{|l,i| FrenchCoordinates.new([l,i])}
      wyp-wkl
  end
  
  def wklesle_rogi
      wyp=(self).each_with_index.to_a.collect{|l,i| FrenchCoordinates.new([l,i+1])}
      wkl=self.each_with_index.to_a.collect{|l,i| FrenchCoordinates.new([l,i])}
      wkl-wyp
  end

  
  
#   def obrys(opcje="")      
#       p [wklesle_rogi,wypukle_rogi].transpose
#       punkty=[wklesle_rogi,wypukle_rogi].transpose.flatten(1) <<  FrenchCoordinates.new([0,0]);
#                  
#       wielokat(punkty,opcje)
#   end

  
#   def obrys_przedluzony(opcje="")      
#       punkty=[FrenchCoordinates.new([ilosc_kolumn+Przedluzenie,0])]+
#       [wypukle_rogi,wklesle_rogi].transpose.flatten(1)[1..-1] << 
#       FrenchCoordinates.new([0,ilosc_wierszy+Przedluzenie]) <<
#       FrenchCoordinates.new([0,0]) 
#                  
#       wielokat(punkty,opcje)
#   end

  
  def profil_przedluzony(opcje="")      
      punkty=[FrenchCoordinates.new([ilosc_kolumn+Przedluzenie,0])]+
      [wklesle_rogi,wypukle_rogi].transpose.flatten(1)  << 
            FrenchCoordinates.new([0,ilosc_wierszy]) <<
      FrenchCoordinates.new([0,ilosc_wierszy+Przedluzenie]) 
                 
      linia(punkty,opcje)
  end



  def pokratkowany(opcje="")
#   na razie tylko konwencja francuska!    
      grafika = ""
      grafika << "\\begin{scope}\n"        
      grafika << clip([wypukle_rogi,wklesle_rogi].transpose.flatten(1)[1..-1] <<  FrenchCoordinates.new([0,0]));
      grafika << "\\draw (0,0) grid (#{ilosc_kolumn},#{ilosc_wierszy});\n"
      grafika << "\\end{scope}\n"         
#       grafika << obrys(opcje)
      grafika
  end



end





# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
class TableauYounga



  def zaznacz_zawartosc
    klatki.collect {|x,y,n| klatka_tableau(x,y,n+1) }.join
  end



  def rysunek(opcje="")
        grafika=""   

        grafika << self.w_Younga.pokratkowany(opcje);
        grafika << zaznacz_zawartosc

        grafika
  end




  def kolorowane_tableau

    podzial=8;

    kolory=%w{ red  orange magenta green cyan yellow pink white}
#     koloryrgb=[ [0.8,0.8,1], [0.7,0.7,1], [0.6,0.6,1],[0.5,0.5,1], [1,0.4, 0], [1,0.6,0.2], [1,0.8,0.4], [1,1,0.6] ];
    koloryrgb=[ [0.8,0.8,1], [0.7,0.7,1], [0.6,0.6,1],[0.5,0.5,1], [0, 1, 0], [0.75,1,0.5], [0.9,1,0.6], [0.9,1,0.8] ];

    
    grafika=""
    
    (1..podzial).to_a.reverse.each do |n|
            #        grafika << "\\psset{fillcolor=#{kolory[n-1]}}\n";
                grafika << "\\newrgbcolor{szary}{#{koloryrgb[n-1][0]} #{koloryrgb[n-1][1]} #{koloryrgb[n-1][2]}} \n"
              
            
                grafika << self.usun_duze_klatki(self.ilosc_klatek*n / podzial).w_Younga.obrys( 
                      "linewidth=0mm,fillcolor=szary,fillstyle=solid"); end
                
    grafika << self.rysunek;
                
    (1..podzial).to_a.reverse.each do |n|
            #        grafika << "\\psset{fillcolor=#{kolory[n-1]}}\n";
                  next if n==4
                  grafika << self.usun_duze_klatki(self.ilosc_klatek*n / podzial).w_Younga.profil("linewidth=0.5mm") 
#                       ([4,8].member?(n) ? "linewidth=1mm" : "linewidth=0.5mm") 
#                          case n
#                             when 4 then "linewidth=1.5mm,linecolor=green"
# #                             when 8 then "linewidth=1mm"
#                             else "linewidth=0.5mm"
#                          end 
                    end 


     grafika << self.usun_duze_klatki(self.ilosc_klatek*4 / podzial).w_Younga.profil_przedluzony([ilosc_wierszy,0],[0,ilosc_kolumn],"linecolor=red,linewidth=2mm") 
     
     grafika
  end;








# kazda klatka w innym kolorze
  def kolorowane_tableau_teczowe

    k=klatki;
    maks=k[-1][-1];

    grafika="" 

    k.each do |x,y,n| 
       grafika << "\\newrgbcolor{szary}{#{(n.to_f / maks).rn} #{(1- n.to_f / maks).rn} 0}\n";
       grafika << wielokat([[x,y],[x+1,y],[x+1,y+1],[x,y+1]],"fillcolor=szary,fillstyle=solid")
    end

#     grafika << zaznacz_zawartosc



    podzial=6;
    
#     (1..podzial).to_a.reverse.each { |n|
#        grafika << "\\newgray{szary}{#{(1000*(0.3* n / (podzial)+0.7)).to_i /  1000.0}}\n";
#        grafika << self.usun_duze_klatki(self.ilosc_klatek*n / podzial).w_Younga.rysunek( 
# 	  "linewidth=0.5mm,fillcolor=szary,fillstyle=solid"); }
     
     grafika << self.rysunek;
     
    (1..podzial).to_a.reverse.each { |n|
#        grafika << "\\newgray{szary}{#{(1000*(0.3* n / (podzial)+0.7)).to_i / 1000.0}}\n";
       grafika << self.usun_duze_klatki_zostaw_zero(self.ilosc_klatek*n / podzial).w_Younga.rysunek( 
	  (true ? "linewidth=1mm" : "linewidth=0.5mm") ); }



    grafika
  end;





end



