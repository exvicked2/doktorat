set table "4-z2.txt"
set format "% .5f"
set samples 100
set parametric

set dummy z

set trange [-2:2]
plot z,  ((4-z**2)**1.5)/(12*pi)